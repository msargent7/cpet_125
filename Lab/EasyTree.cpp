// easy tree 

#include <iostream>
using namespace std;

typedef struct _NODE_ { 
    int data;
    _NODE_* left; 
    _NODE_* right; 
}NODE, *PNODE;
  
bool isLeaf(PNODE node); 
void  createTree(PNODE * tree) { *tree = NULL; }
PNODE createNode(int data);
PNODE insert(PNODE root, int data); 
void  destroyTree(PNODE node);
void  printTree(PNODE root); 
bool containsValue(NODE * tree, int data);
bool minValue(NODE * tree, int & data);
int size(NODE * tree);
void copyTree(NODE ** dest, NODE * src);
int maxDepth(NODE * tree);

int main()
{
	NODE * tree = 0; 

	createTree(&tree); 

	tree = insert(tree, 5);
	tree = insert(tree, 3);
	tree = insert(tree, 8);
	tree = insert(tree, 2);
	tree = insert(tree, 4);
	tree = insert(tree, 6);
	tree = insert(tree, 9);

	printTree(tree); 

	cout << endl; 

	destroyTree(tree); 

	return 0; 
}

// FUNCTION: isLeaf
// INPUTS: tree node, or NULL
// OUTPUT: true if the node is a leaf node, false otherwise
bool isLeaf(PNODE node)
{
	if (!node) return false; 

	if (node->left == nullptr && node->right == nullptr) return true; 

	return false; 
}

PNODE createNode(int data)
{
	NODE * temp = new NODE;
	temp->left = 0; 
	temp->right = 0; 
	temp->data = data; 

	return temp; 
}







PNODE insert(PNODE root, int data) 
{
	if(root == NULL) // empty tree
	{
		// creat the tree
		PNODE pTemp = createNode(data); 

		// return the new root
		return pTemp; 
	}
	else // not empty tree
	{
		if(data <= root->data)
		{
			// insert on left side
			root->left = insert(root->left, data); 
		}
		else
		{
			// insert on right side
			root->right = insert(root->right, data); 
		}
	}

	return root; 
} 

void destroyTree(PNODE node)
{
	if(node != NULL)
	{
		// destroy the left side of the tree
		destroyTree(node->left); 

		// destroy the right side of the tree
		destroyTree(node->right); 

		// output for debug purposes only
		cout << "deleting " << node->data << endl; 

		// at this point, the tree has neither a left or a right
		delete node; 
	}
}

void  printTree(PNODE root)
{
	if(root != NULL)
	{
		printTree(root->left); 
		cout << root->data << endl; 
		printTree(root->right); 
	}
}

bool containsValue(NODE * tree, int data)
{
	if (tree = nullptr)
	{
		return false;
	}

	else
	{

		if (data == tree->data)
		{
			return true;
		}

		else if (data < tree->data)
		{
			return containsValue(tree->left, data);
		}

		else if (data > tree->data)
		{
			return containsValue(tree->right, data);
		}
	}
}
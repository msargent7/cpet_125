#ifndef _RECT_H_
#define _RECT_H_

class rect {
private:
	double _height;
	double _width;

public:

	rect() : _height(0), _width(0)
	{
	}

	rect(int width, int height) : _height(height), _width(width)
	{
	}

	void set(double width, double height);

	double height();

	double width();

	double area();

	// ***** additional functions *****

	bool operator>(rect& a) 
	{
		return this->area() > a.area();
	}

};


#endif

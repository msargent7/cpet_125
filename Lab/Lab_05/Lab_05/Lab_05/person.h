#ifndef _PERSON_H_
#define _PERSON_H_

class person {
private:
	int _age;

public:

	person() : _age(0)
	{
	}

	person(int age) : _age(age)
	{
	}

	void set(int age);

	int age();

	bool operator>(person& a)
	{
		return this->age() > a.age();
	}
};

#endif
#include <iostream>
#include <ctime> // for random
using namespace std;

#include "rect.h"
#include "person.h"

int random(int min, int max);

template<class T>
void _sort(T * a, int n);

// **** additional functions **** 

const int ARR_SIZE = 10;

int main()
{
	rect arr[ARR_SIZE]; // initialize this array of pointers
	person personArr[ARR_SIZE];

	for (int i = 0; i < ARR_SIZE; i++)
	{
		arr[i].set(random(1, 200), random(1, 200));
		personArr[i].set(random(1, 70));
	}

	cout << "Current array contents: " << endl;
	for (int i = 0; i < ARR_SIZE; i++)
	{
		cout << "arr[" << i << "] area = " << arr[i].area() << endl;
	}
	for (int i = 0; i < ARR_SIZE; i++)
	{
		cout << "arr[" << i << "] age = " << personArr[i].age() << endl;
	}

	_sort(arr, ARR_SIZE);
	_sort(personArr, ARR_SIZE);

	cout << "Sorted array contents: " << endl;
	for (int i = 0; i < ARR_SIZE; i++)
	{
		cout << "arr[" << i << "] area = " << arr[i].area() << endl;
	}
	for (int i = 0; i < ARR_SIZE; i++)
	{
		cout << "arr[" << i << "] age = " << personArr[i].age() << endl;
	}

	return 0;
}

int random(int min, int max)
{
	static bool ft = true;

	if (ft)
	{
		srand((unsigned int)time(0));
		ft = false;
	}

	return (rand() % (max - min + 1)) + min;
}

template<class T>
void _sort(T * a, int n)
{
	for (int i = 0; i < n; i++)
	{
		int min = i;

		for (int p = i; p < n; p++)
		{
			if (a[min] > a[p])
			{
				min = p;
			}
		}

		T temp = a[i];

		a[i] = a[min];

		a[min] = temp;
	}
}

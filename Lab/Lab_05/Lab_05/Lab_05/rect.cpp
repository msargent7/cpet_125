#include "rect.h"

void rect::set(double width, double height)
{
	_height = height;
	_width = width;
}

double rect::height()
{
	return _height;
}

double rect::width()
{
	return _width;
}

double rect::area()
{
	return _height * _width;
}


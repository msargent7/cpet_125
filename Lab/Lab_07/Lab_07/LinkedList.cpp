/*


Instructions:
Please insert the standard header above.
Add the functions in the ADD THESE FUNCTIONS sections below.
Include function descriptions for all your functions.
Add to the input enum to create additional commands.
Do not modify the way the list works.
You may want to add an input to print the menu.
Demonstrate the results to the instructor.

Here is how the list works:

head or front                         tail or back

list
|---> 100
next ->		200
next ->  300
next ->  400
next -> NULL
*/

#include <iostream>
using namespace std;

enum INPUTS {
	ADD_NODE,
	PRINT_LIST,
	REMOVE_FIRST_NODE,
	// INSERT YOUR ENUMS HERE - Make sure you change the menu function
	PRINT_LIST_RECURSIVE,
	REVERSE_RECURSIVE,
	ADD_NODE_BACK,
	FRONT_OF_LIST,
	BACK_OF_LIST,
	IS_IN_LIST,
	REMOVE_FROM_LIST,
	INSERT_NODE_AT,
	REMOVE_NODE_AT,
	DESTROY_LIST,
	END_APP // keep this as your last enum
};

typedef struct _NODE_ {

	_NODE_ * next;
	int data;

}NODE, *PNODE;

void menu();

void printList(NODE * pList);
void createList(NODE **pList);
NODE * destroyList(NODE * pList);

NODE * pushFront(NODE * pList, NODE * pNode);
NODE * popFront(NODE * pList);

NODE * createNode(int value);

// ADD THESE FUNCTIONS (10 points)
void printListRecursive(NODE * pList); // implementes print recursively
void printBackwards(NODE * pList);     // must be recursive
NODE * pushBack(NODE * pList, NODE * pNode); // adds a node to the back of the list
NODE * front(NODE * pList); // returns the node at the front of the list
NODE * back(NODE * pList);  // returns the node at the back of the list

							// add these functions too (15 points each)
bool isInList(NODE * pList, int value); // returns true if value is in list, false otherwise
NODE * removeFromList(NODE * pList, int value); // Returns the modified list. 

												// Add these function AFTER completing the above (10 points each)
												// Use the above functions to help you complete these.  You may also
												// create other functions to help you.  When divided into smaller tasks,
												// these functions are actually easy.  Both functions return the modified list. 
NODE * insertAt(NODE * pList, NODE * pNode, int index);
NODE * removeAt(NODE * pList, int index);

int main(void)
{
	NODE * list;
	NODE * node;
	NODE * frontNode;
	NODE * backNode;
	int input;
	int count = 10;
	int searchValue;
	int removeValue;
	int insertIndex;
	int removeIndex;

	createList(&list);

	menu();

	while (1)
	{
		cin.clear(); // clear error bits
		cin.sync();  // empty buffer

		cout << endl << "INPUT: ";
		cin >> input;  // valid inputs only -- no error checking!

		switch (input)
		{
		case ADD_NODE:
			node = createNode(count++);
			list = pushFront(list, node);
			break;
		case PRINT_LIST:
			printList(list);
			break;
		case REMOVE_FIRST_NODE:
			list = popFront(list);
			break;
		case PRINT_LIST_RECURSIVE:
			printListRecursive(list);
			break;
		case REVERSE_RECURSIVE:
			printBackwards(list);
			break;
		case ADD_NODE_BACK:
			node = createNode(count++);
			list = pushBack(list, node);
			break;
		case FRONT_OF_LIST:
			frontNode = front(list);
			cout << frontNode->data << endl;
			break;
		case BACK_OF_LIST:
			backNode = back(list);
			cout << backNode->data << endl;
			break;
		case IS_IN_LIST:
			cout << "Enter value to search for: ";
			cin >> searchValue;
			cout << endl;
			cout << isInList(list, searchValue);
			break;
		case REMOVE_FROM_LIST:
			cout << "Enter the node you wish to remove: ";
			cin >> removeValue;
			cout << endl;
			list = removeFromList(list, removeValue);
			break;
		case INSERT_NODE_AT:
			cout << "Enter the index where you would like to add a node: ";
			cin >> insertIndex;
			cout << endl;
			node = createNode(count++);
			list = insertAt(list, node, insertIndex);
			break;
		case REMOVE_NODE_AT:
			cout << "Enter the index where you would like to remove a node: ";
			cin >> removeIndex;
			cout << endl;
			removeAt(list, removeIndex);
			break;
		case DESTROY_LIST:
			destroyList(list);
			break;
		case END_APP:
			list = destroyList(list);
			return 0;
		default:
			cout << "Invalid input" << endl;
		}
	}

	return 0;
}

/*
FUNCTION:		menu
PRECONDITION:	None
POSTCONDITION:	The program menu is output.
*/
void menu()
{
	cout << endl;
	cout << "Inputs: " << endl;
	cout << ADD_NODE << " add a node " << endl;
	cout << PRINT_LIST << " print list " << endl;
	cout << REMOVE_FIRST_NODE << " remove first node " << endl;
	cout << PRINT_LIST_RECURSIVE << " print list recursively" << endl;
	cout << REVERSE_RECURSIVE << " reverse list recursively" << endl;
	cout << ADD_NODE_BACK << " add node to end of list" << endl;
	cout << FRONT_OF_LIST << " return the front node of the list" << endl;
	cout << BACK_OF_LIST << " return the back node of the list" << endl;
	cout << IS_IN_LIST << " search for a value in the list" << endl;
	cout << REMOVE_FROM_LIST << " remove a node from the list" << endl;
	cout << INSERT_NODE_AT << " insert a node at the given index" << endl;
	cout << REMOVE_NODE_AT << " remove a node at the given index" << endl;
	cout << DESTROY_LIST << " destroy the list" << endl;

	// insert additional enums here


	cout << END_APP << " End application " << endl;
}

/*
FUNCTION:		createList
PRECONDITION:	Address of a list pointer.
POSTCONDITION:	pList points to a valid node.
*/
void createList(NODE **pList)
{
	(*pList) = NULL; // empty list
}

/*
FUNCTION:		destroyList
PRECONDITION:	pList is a valid list pointer.
POSTCONDITION:	The list pointer and all nodes in the list are destroyed.
Returns the empty list.
*/
NODE * destroyList(NODE * pList)
{
	while (pList = popFront(pList));

	return NULL;
}

/*
FUNCTION:		pushFront
PRECONDITION:	pList is a valid list pointer.  pNode is a valid node pointer.
POSTCONDITION:	pNode is inserted at the front of the list.  Returns the new list.
*/
NODE * pushFront(NODE * pList, NODE * pNode)
{
	if (pList == NULL) // no list yet
	{
		return pNode;
	}
	else // the list exists
	{
		pNode->next = pList;
		pList = pNode;

		return pList;
	}
}

/*
FUNCTION:		popFront
PRECONDITION:	pList is a valid list pointer, or NULL.
POSTCONDITION:	returns a new list if there are nodes in the list, NULL (empty list) otherwise.
*/
NODE * popFront(NODE * pList)
{
	NODE * temp;

	if (!pList) return NULL;

	temp = pList; // point to first node in list

	pList = pList->next;

	delete temp;

	return pList;
}

/*
FUNCTION:		createNode
PRECONDITION:	A value to store in the list.
POSTCONDITION:	Returns a pointer to a node containing the value.
*/
NODE * createNode(int value)
{
	NODE * temp = new NODE;
	temp->data = value;
	temp->next = 0;
	return temp;
}

/*
FUNCTION:		printList
PRECONDITION:	pList is a valid list pointer, or NULL.
POSTCONDITION:	If the list exists, outputs the list to the console.
*/
void printList(NODE * pList)
{
	NODE * temp;

	if (!pList) return;

	temp = pList; // start with first node

	while (temp != NULL)
	{
		cout << temp->data << endl;
		temp = temp->next;
	}

}

void printListRecursive(NODE * pList)
{
	if (pList == NULL)
	{
		cout << endl;
	}
	else
	{
		cout << pList->data << endl;
		pList = pList->next;
		printListRecursive(pList);
	}
}

void printBackwards(NODE * pList)
{
	if (pList == NULL)
	{
		cout << endl;
	}
	else
	{
		printBackwards(pList->next);
		cout << pList->data << endl;
	}
}

NODE * pushBack(NODE * pList, NODE * pNode)
{
	if (pList == NULL) // no list yet
	{
		return pNode;
	}

	else
	{
		NODE * temp = pList;
		while (temp->next)
		{
			temp = temp->next;
		}

		temp->next = pNode;

		return pList;
	}
}

NODE * front(NODE * pList)
{
	return pList;
}

NODE * back(NODE * pList)
{
	NODE * backNode;

	while (pList->next != NULL)
	{
		pList = pList->next;
	}

	backNode = pList;
	return backNode;
}

bool isInList(NODE * pList, int value)
{
	while (pList->next != NULL)
	{
		if (pList->data == value)
		{
			return true;
		}
		else
		{
			pList = pList->next;
		}
	}
	return false;
}

NODE * removeFromList(NODE * pList, int value)
{

	NODE * temp = pList;

	while (temp->next != NULL)
	{
		if (temp->next->data == value)
		{
			temp->next = temp->next->next;
			return pList;
		}
		else
		{
			temp = temp->next;
		}
	}
	return pList;
}

NODE * insertAt(NODE * pList, NODE * pNode, int index)
{
	int count = 0;
	NODE * temp = pList;
	NODE * prev = pList;

	if (index == 0)
	{
		pNode->next = temp;
		return pList;
	}

	while (temp->next != NULL)
	{
		if (count == index)
		{
			pNode->next = temp;
			prev->next = pNode;
			return pList;
		}
		prev = temp;
		temp = temp->next;
		count++;
	}

	return pList;
}

NODE * removeAt(NODE * pList, int index)
{
	int count = 0;
	NODE * temp = pList;

	if (index == 0)
	{
		temp = temp->next;
		return pList;
	}

	while (temp->next != NULL)
	{
		if (count == index)
		{
			temp->next = temp->next->next;
			return pList;
		}
		temp = temp->next;
		count++;
	}

	return pList;
}
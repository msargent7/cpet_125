/*
* File: AG121_Lab02_Functions_Interface.h 
* -----------------
* Interface file for AG121 Lab02.  Function prototypes supplied here. 
*
* Non-template functions should be implemented in AG121_Lab02_Functions_Interface.cpp
*
* Template functions should be implemented in this file
*/

#ifndef _AG121_LAB02_FUNCTIONS_H_
#define _AG121_LAB02_FUNCTIONS_H_

#include <iostream>
using namespace std; 

/*
* Function: getNumbers
* Usage: 
*	int arr[10]; 
*   int size = 10; 
*
*	getNumbers(arr, size);  
*
* ------------------------------------
* This function populates a with numbers up to 
* size array elements.  When the function returns, 
* size contains the number of numbers stored in a. 
*/
void getNumbers(int * a, int & size); 

/*
* Function: factorial
* Usage: 
*	int r = factorial(3); 
* ------------------------------------
* Returns n factorial
*/
int factorial(int n); 

/*
* Function: factorialRecursive
* Usage: 
*	int r = factorialRecursive(3); 
* ------------------------------------
* Returns n factorial.  The algorithm is implemented using recursion. 
*/
int factorialRecursive(int n); 

/*
* Function: printString
* Usage: 
*	printString("hello"); 
* ------------------------------------
* Prints a C-String to the console. 
*/
void printString(char * s); 

/*
* Function: printStringRecursive
* Usage: 
*	printStringRecursive("hello"); 
* ------------------------------------
* Uses recursion to print a C-String to the console. 
*/
void printStringRecursive(char * s); 

/*
* Function: printStringBackwards
* Usage: 
*	printStringBackwards("hello"); 
* ------------------------------------
* Prints a C-String to the console, backwards
*/
void printStringBackwards(char * s); 

/*
* Function: printStringBackwardsRecursive
* Usage: 
*	printStringBackwardsRecursive("hello"); 
* ------------------------------------
* Uses recursion to print a C-String to the console in reverse order. 
*/
void printStringBackwardsRecursive(char * s); 

/*
* Function: swapWithReferences
* Usage: 
*	int first = 1, second = 2; 
*	swapWithReferences(first, second); 
* ------------------------------------
* This function swaps the values in a and b.  
*/
template <class T>
void swapWithReferences(T & a, T & b)
{
	T temp = a; 
	a = b; 
	b = temp; 
}

/*
* Function: swapWithPointers
* Usage: 
*	int first = 1, second = 2; 
*	swapWithReferences(&first, &second); 
* ------------------------------------
* This function swaps the values in a and b.  
*/
template <class T>
void swapWithPointers(T * a, T * b)
{
	 T temp = *a;
	 *a = *b;
	 *b = temp;
}

/*
* Function: lowest
* Usage: 
*	int high = lowest<int>(1, 2, 3);  
* ------------------------------------
* This function returns the input with the lowest value. 
*/
template <class T>
T lowest(T a, T b, T c)
{
	T _lowest = a;

	if ( a > b && b < c)
	{
		_lowest = b;
	}
	else if (b > c)
	{
		_lowest = c;
	}

	return _lowest;  
}

/*
* Function: highest
* Usage: 
*	int high = heighest<int>(1, 2, 3);  
* ------------------------------------
* This function returns the input with the greatest value. 
*/
template <class T>
T highest(T a, T b, T c)
{
	T _highest = a;

	if (a < b && b > c)
	{
		_highest = b;
	}
	if (b < c)
	{
		_highest = c;
	}
	
	return _highest;  
	
}

/*
* Function: init
* Usage: 
*	int arr[10]; 
*	init<int>(arr, 10, -1); 
* ------------------------------------
* This function sets every element in the array a to the value v.
* size indicates the number of elements in a.  
*/
template <class T>
void init(T * a, int size, T v)
{
	 for (int i = 0; i < size; i++)
	 {
		 a[i] = v;
	 }
}

#endif 
/*
* File: AG121_Lab02_Functions_Implementation.cpp 
* -----------------
* Implementation file for AG121 Lab02.  The student should
* supply function definitions here
*/

#include "AG121_Lab02_Functions_Interface.h"

void getNumbers(int * a, int & size)
{
	for (int i = 0; i < size; i++)
	{
		a[i] = 7;
	}
}

int factorial(int n)
{
	int total = 1; 

	for(int i = 1; i <= n; i++)
	{
		total *= i; 
	}

	return total; 
}

int factorialRecursive(int n)
{
	if(n == 0)
	{
		return 1;  // every recursive function must have 
	}
	
	return n * factorialRecursive(n - 1); 
}

void printString(char * s)
{
	while(*s != NULL)
	{
		cout << *s; 
		s++; 
	}
}

void printStringRecursive(char * s)
{

	if (*s == NULL)
	{
		cout << endl;
	}
	else
	{
		cout << *s;
		*s++;
		printStringRecursive(s);
	}
}

void printStringBackwards(char * s)
{
	int _length = strlen(s);

	for (int i = _length-1; i >=0; i--)
	{
		cout << s[i];
	}
}

void printStringBackwardsRecursive(char * s)
{
	if (*s == NULL)
	{
		return;
	}
	else
	{
		printStringBackwardsRecursive(s+1);
		cout << *s;
	}
}



/*
FILE: <the name of the file>
PROGRAMMER: <your name>
DATE: <the date the project is submitted>
PROJECT: <the name of the project >
CREDITS: <A list of web sites, books, and programmers used to create the code.  You do not need to list any text book used in this course or the instructors name. >
LIMITATIONS: <Any limitations your program may have.  For example, Your  program might fail if the user inputs a number instead of a string.>
*/


#include <iostream>
using namespace std;

#include "AG121_Lab02_Functions_Interface.h"

void testGetNumbers(); 
void testFactorial(); 
void testStringPrinting(); 
void testSwaps(); 
void testLowestHighest(); 

int main()
{
	cout << "This program tests several fuctions for AG121 Lab 02. " << endl; 

	testGetNumbers(); 
	testFactorial(); 
	testStringPrinting(); 
	testSwaps(); 
	testLowestHighest(); 

	system("pause"); 

	return 0; 
}

void testGetNumbers()
{
	int arr[5]; 
	int size = 5; 

	cout << "Testing init and getNumbers." << endl; 

	init(arr, 5, -1); 

	cout << "Before getNumbers: " << endl; 
	for(int i = 0; i < size; i++)
	{
		cout << "arr[" << i << "] = " << arr[i] << endl; 
	}

	getNumbers(arr, size); 

	cout << "After getNumbers: " << endl; 
	for(int i = 0; i < size; i++)
	{
		cout << "arr[" << i << "] = " << arr[i] << endl; 
	}

	cout << endl; 
}

void testFactorial()
{
	int n = 5; 

	cout << "testing n! using factorial and factorialRecursive: " << endl;
	cout << "n! = " << factorial(n) << endl; 
	cout << "n! = " << factorialRecursive(n) << endl; 

	cout << endl; 
}

void testStringPrinting()
{
	cout << "Testing string functions: " << endl; 

	cout << "The string output is: "; 
	printString("Hello world"); 
	cout << endl; 

	cout << "The string output is: "; 
	printStringRecursive("Hello world"); 
	cout << endl; 

	cout << "The string output backwards is: "; 
	printStringBackwards("Hello world"); 
	cout << endl; 
	

	cout << "The string output backwards is: "; 
	printStringBackwardsRecursive("Hello world"); 
	cout << endl; 

	cout << endl; 
	
}

void testSwaps()
{
	int first = 1; 
	int second = 2; 

	cout << "Test swaps." << endl; 

	cout << "First  = " << first << endl; 
	cout << "Second = " << second << endl; 

	swapWithReferences(first, second); 

	cout << "After swap..." << endl; 
	cout << "First  = " << first << endl; 
	cout << "Second = " << second << endl; 

	swapWithPointers(&first, &second); 

	cout << "After swap again..." << endl; 
	cout << "First  = " << first << endl; 
	cout << "Second = " << second << endl; 

	cout << endl; 
}

void testLowestHighest()
{
	int a = 2, b = 10, c = 20; 
	int low, high; 

	low = lowest<int>(a, b, c); // you can specify the type
	high = highest(a, b, c);    // the compiler figures out the type

	cout << "Test lowest and highest with the numbers " << a << ", " << b << ", and " << c << endl; 
	cout << "The lowest number is: " << low << endl; 
	cout << "The highest number is: " << high << endl; 

	cout << endl; 

}

/*
ABSTRACT:
Trivial AVL Tree balancing example.

CREDIT:
The code in this program is from BST.H, coded at Standford University.  It is part
of a course in computer science that uses the electronic text book: 

Programming Abstractions in C++: A Second Course in Computer Science
Eric S. Roberts and Julie Zelenski (Authors)

This text can be found at:

http://www.stanford.edu/class/cs106b/materials/CS106BX-Reader.pdf

The code has been modified to use pointers to pointers instead of references to pointers. 

*/

#include <iostream> 
using namespace std;

typedef struct _NODE_ {
	int data;		// int will do here; could use a template
	_NODE_ * left; 
	_NODE_ * right; 
	int bf;			// balance factor
}NODE, *PNODE; 

NODE * createNode(int data); 

bool insertAVL(NODE ** pTree, int data);
void updateBF (NODE ** pTree, int delta); 

void fixRightImbalance(NODE ** pTree); 
void fixLeftImbalance(NODE ** pTree); 
void rotateRight(NODE ** pTree); 
void rotateLeft(NODE ** pTree); 

void displayNode(NODE * treeNode, bool showChildren = false); 

void displayTree(NODE * tree); 
void destroyTree(NODE * tree);

const int RIGHT_HEAVY = +1; 
const int EVEN = 0; 
const int LEFT_HEAVY = -1; 

int main()
{
	NODE * tree = NULL; // start with empty tree 

	insertAVL(&tree, 20); 
	insertAVL(&tree, 25); 
	insertAVL(&tree, 30); 

	displayTree(tree);

	destroyTree(tree); 


	return 0; 

}// end main

NODE * createNode(int data)
{
	NODE * temp = new NODE; 

	temp->data = data; 
	temp->bf = EVEN; 
	temp->left = NULL; 
	temp->right = NULL; 

	return temp; 
}

bool insertAVL(NODE ** pTree, int data)
{
	if((*pTree) == NULL)
	{
		// all insert operations eventually end up here
		(*pTree) = createNode(data); 

		return true; // height has increased
	}

	if((*pTree)->data == data)
	{
		// no duplicate values
		return false; // no increase in height
	}

	int bfDelta = 0; 

	if(data < (*pTree)->data)
	{
		if( insertAVL(&((*pTree)->left), data) == true )
		{
			bfDelta = -1; // just added a node on the left (sub)tree
		}
	}
	else // data > t->data
	{
		if( insertAVL(&((*pTree)->right), data) == true )
		{
			bfDelta = 1; // just added a node on the right (sub)tree
		}
	}

	updateBF(pTree, bfDelta); 

	if(bfDelta != 0 && (*pTree)->bf != EVEN)
	{
		return true; 
	}

	return false; 

}

void updateBF (NODE ** pTree, int delta)
{
	// bf will be -1, 0, or 1.  delta will be -1, 0, or 1. 
	(*pTree)->bf += delta;  

	if((*pTree)->bf < LEFT_HEAVY)
	{
		fixLeftImbalance(pTree); 
	}
	else if((*pTree)->bf > RIGHT_HEAVY)
	{
		fixRightImbalance(pTree); 
	}

}

void fixRightImbalance(NODE ** pTree)
{
	NODE * child = (*pTree)->right; 

	if(child->bf == LEFT_HEAVY)
	{
		int oldBF = child->left->bf; 

		rotateRight(&(*pTree)->right); 
		rotateLeft(pTree); 

		(*pTree)->bf = EVEN; 

		switch(oldBF)
		{
		case LEFT_HEAVY:

			(*pTree)->left->bf = EVEN; 
			(*pTree)->right->bf = RIGHT_HEAVY; 

			break; 

		case EVEN:

			(*pTree)->left->bf = (*pTree)->right->bf = EVEN; 

			break; 

		case RIGHT_HEAVY:

			(*pTree)->left->bf = LEFT_HEAVY; 
			(*pTree)->right->bf = EVEN; 

			break; 


		}// end switch 

	}
	else if(child->bf == EVEN)
	{
		rotateLeft(pTree); 
		(*pTree)->bf = LEFT_HEAVY; 
		(*pTree)->left->bf = RIGHT_HEAVY; 
	}
	else // child is right heavy
	{
		rotateLeft(pTree); 
		(*pTree)->left->bf = (*pTree)->bf = EVEN; 
	}
}

void rotateRight(NODE ** pTree)
{

	NODE * parent = (*pTree); 
	NODE * child = parent->left; 

	parent->left = child->right; 
	child->right = parent; 

	(*pTree) = child; 
}

void rotateLeft(NODE ** pTree)
{
	NODE * parent = (*pTree); 
	NODE * child = parent->right; 

	parent->right = child->left; 
	child->left = parent; 

	(*pTree) = child; 

}

void fixLeftImbalance(NODE ** pTree) 
{
	NODE * child = (*pTree)->left; 

	if(child->bf == RIGHT_HEAVY)
	{
		int oldBF = child->right->bf; 

		rotateLeft(&(*pTree)->left); 
		rotateRight(pTree); 

		(*pTree)->bf = EVEN; 

		switch(oldBF)
		{
		case LEFT_HEAVY:

			(*pTree)->left->bf = EVEN; 
			(*pTree)->right->bf = RIGHT_HEAVY; 

			break; 

		case EVEN:

			(*pTree)->left->bf = (*pTree)->right->bf = EVEN; 

			break; 

		case RIGHT_HEAVY:

			(*pTree)->left->bf = LEFT_HEAVY; 
			(*pTree)->right->bf = EVEN; 

			break; 

		}// end switch

	}
	else if(child->bf == EVEN)
	{
		rotateRight(pTree); 
		(*pTree)->bf = RIGHT_HEAVY; 
		(*pTree)->right->bf = LEFT_HEAVY; 
	}
	else
	{
		rotateRight(pTree); 
		(*pTree)->right->bf = (*pTree)->bf = EVEN; 
	}
}



void displayTree(NODE * tree)
{
	if(tree)
	{
		displayTree(tree->left); 
		displayNode(tree, true);  
		displayTree(tree->right); 
	}
}

void displayNode(NODE * treeNode, bool showChildren)
{
	cout << treeNode->data; 

	if(showChildren)
	{
		cout << "("; 

		if(treeNode->left)
		{
			cout << treeNode->left->data; 
		}
		else
		{
			cout << "NULL"; 
		}

		cout << ", "; 

		
		if(treeNode->right)
		{
			cout << treeNode->right->data; 
		}
		else
		{
			cout << "NULL"; 
		}

		cout << ") "; 


	}

	cout << endl; 

}

void destroyTree(NODE * tree)
{
	if(tree != NULL)
	{
		// destroy the left side of the tree
		destroyTree(tree->left); 

		// destroy the right side of the tree
		destroyTree(tree->right); 

		// output for debug purposes only
		cout << "deleting " << tree->data << endl; 

		// at this point, the tree has neither a left or a right
		delete tree; 
	}
}
#ifndef _BINARY_TREE_
#define _BINARY_TREE_ 

#include "treebase.h"



class binaryTree : public ITree {
private:

	static const int RIGHT_HEAVY = +1; 
	static const int EVEN = 0; 
	static const int LEFT_HEAVY = -1; 

	NODE * _root; 

	NODE * createNode(int data); 

	int  insertAVL(NODE ** pTree, int data);
	void fixRightImbalance(NODE ** pTree); 
	void fixLeftImbalance(NODE ** pTree); 
	void rotateRight(NODE ** pTree); 
	void rotateLeft(NODE ** pTree); 

	void deepCopy(binaryTree & copyMe); 

	void destroyTree(NODE * tree); 
	void copyTree(NODE ** dest, NODE * src); 
	int size(NODE * root); 

	void updateBF (NODE ** pTree, int delta);

	PNODE insert(PNODE root, int data);

public:

	binaryTree(); 
	binaryTree(binaryTree & right); 
	~binaryTree(); 

	void operator=(binaryTree & right); 

	void insert(int data);		  // inserts a value into the tree
	void clear();				  // empties the tree
	bool containsValue(int data); // true if data is in the tree, false otherwise
	int  size();				  // count of nodes in tree
	bool minValue(int & data); 
	int  maxDepth(); 
	bool isEmpty();			      // true if root == NULL 

	NODE * getRoot();					 // returns the root of the tree for tree traversal 
}; 

#endif 

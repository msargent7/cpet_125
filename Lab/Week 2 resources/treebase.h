#ifndef _TREE_BASE_
#define _TREE_BASE_

typedef struct _NODE_ {
	int data;
	_NODE_* left;
	_NODE_* right;
	int bf;
}NODE, *PNODE;

class ITree 
{     

// This class defines the minimum functionality required of any class that 
// inherits (impements) this class (interface)

public:         
	virtual NODE * getRoot() = 0; 
	virtual void insert(int data) = 0;
	virtual ~ITree() { }
};   

#endif 
 
#include <iostream>
#include "binaryTree.h"
#include "treePrinter.h"
using namespace std;

PNODE binaryTree::createNode(int data)
{
	NODE * temp = new NODE;
	temp->left = nullptr;
	temp->right = nullptr;
	temp->data = data;

	return temp;
}

bool binaryTree::insertAVL(NODE ** pTree, int data)
{
	if ((*pTree) == NULL)
	{
		// all insert operations eventually end up here
		(*pTree) = createNode(data);

		return true; // height has increased
	}

	if ((*pTree)->data == data)
	{
		// no duplicate values
		return false; // no increase in height
	}

	int bfDelta = 0;

	if (data < (*pTree)->data)
	{
		if (insertAVL(&((*pTree)->left), data) == true)
		{
			bfDelta = -1; // just added a node on the left (sub)tree
		}
	}
	else // data > t->data
	{
		if (insertAVL(&((*pTree)->right), data) == true)
		{
			bfDelta = 1; // just added a node on the right (sub)tree
		}
	}

	updateBF(pTree, bfDelta);

	if (bfDelta != 0 && (*pTree)->bf != EVEN)
	{
		return true;
	}

	return false;

}

void binaryTree::fixRightImbalance(NODE ** pTree)
{
	NODE * child = (*pTree)->right;

	if (child->bf == LEFT_HEAVY)
	{
		int oldBF = child->left->bf;

		rotateRight(&(*pTree)->right);
		rotateLeft(pTree);

		(*pTree)->bf = EVEN;

		switch (oldBF)
		{
		case LEFT_HEAVY:

			(*pTree)->left->bf = EVEN;
			(*pTree)->right->bf = RIGHT_HEAVY;

			break;

		case EVEN:

			(*pTree)->left->bf = (*pTree)->right->bf = EVEN;

			break;

		case RIGHT_HEAVY:

			(*pTree)->left->bf = LEFT_HEAVY;
			(*pTree)->right->bf = EVEN;

			break;


		}// end switch 

	}
	else if (child->bf == EVEN)
	{
		rotateLeft(pTree);
		(*pTree)->bf = LEFT_HEAVY;
		(*pTree)->left->bf = RIGHT_HEAVY;
	}
	else // child is right heavy
	{
		rotateLeft(pTree);
		(*pTree)->left->bf = (*pTree)->bf = EVEN;
	}
}

void binaryTree::fixLeftImbalance(NODE ** pTree)
{
	NODE * child = (*pTree)->left;

	if (child->bf == RIGHT_HEAVY)
	{
		int oldBF = child->right->bf;

		rotateLeft(&(*pTree)->left);
		rotateRight(pTree);

		(*pTree)->bf = EVEN;

		switch (oldBF)
		{
		case LEFT_HEAVY:

			(*pTree)->left->bf = EVEN;
			(*pTree)->right->bf = RIGHT_HEAVY;

			break;

		case EVEN:

			(*pTree)->left->bf = (*pTree)->right->bf = EVEN;

			break;

		case RIGHT_HEAVY:

			(*pTree)->left->bf = LEFT_HEAVY;
			(*pTree)->right->bf = EVEN;

			break;

		}// end switch

	}
	else if (child->bf == EVEN)
	{
		rotateRight(pTree);
		(*pTree)->bf = RIGHT_HEAVY;
		(*pTree)->right->bf = LEFT_HEAVY;
	}
	else
	{
		rotateRight(pTree);
		(*pTree)->right->bf = (*pTree)->bf = EVEN;
	}
}

void binaryTree::rotateRight(NODE ** pTree)
{

	NODE * parent = (*pTree);
	NODE * child = parent->left;

	parent->left = child->right;
	child->right = parent;

	(*pTree) = child;
}

void binaryTree::rotateLeft(NODE ** pTree)
{
	NODE * parent = (*pTree);
	NODE * child = parent->right;

	parent->right = child->left;
	child->left = parent;

	(*pTree) = child;

}

int binaryTree::size(NODE * tree)
{
	int sizeOf = 0;

	if (tree == nullptr)
		return sizeOf;

	else
		sizeOf = size(tree->left) + size(tree->right) + 1;

	return sizeOf;
}

void binaryTree::updateBF(NODE ** pTree, int delta)
{
	// bf will be -1, 0, or 1.  delta will be -1, 0, or 1. 
	(*pTree)->bf += delta;

	if ((*pTree)->bf < LEFT_HEAVY)
	{
		fixLeftImbalance(pTree);
	}
	else if ((*pTree)->bf > RIGHT_HEAVY)
	{
		fixRightImbalance(pTree);
	}

}

PNODE binaryTree::insert(PNODE root, int data)
{
	if (root == NULL) // empty tree
	{
		// creat the tree
		PNODE pTemp = createNode(data);

		// return the new root
		return pTemp;
	}
	else // not empty tree
	{
		if (data <= root->data)
		{
			// insert on left side
			root->left = insert(root->left, data);
		}
		else
		{
			// insert on right side
			root->right = insert(root->right, data);
		}
	}

	return root;
}

void binaryTree::insert(int data)
{
	_root = insert(_root, data);
}

binaryTree::binaryTree()
{
	_root = nullptr;
}

binaryTree::binaryTree(binaryTree & right)
{
	deepCopy(right);
}

binaryTree::~binaryTree()
{
	delete _root;
}

void binaryTree::clear()
{
	destroyTree(_root);
	_root = nullptr;
}

void binaryTree::destroyTree(NODE * node)
{
	if (node != NULL)
	{
		// destroy the left side of the tree
		destroyTree(node->left);

		// destroy the right side of the tree
		destroyTree(node->right);

		// output for debug purposes only
		cout << "deleting " << node->data << endl;

		// at this point, the tree has neither a left or a right
		delete node;
	}
}

bool binaryTree::containsValue(NODE * tree, int data)
{
	if (tree == nullptr)
	{
		return false;
	}

	else
	{
		if (data == tree->data)
		{
			return true;
		}

		else if (data < tree->data)
		{
			return containsValue(tree->left, data);
		}

		else if (data > tree->data)
		{
			return containsValue(tree->right, data);
		}
	}

	return false;
}

bool binaryTree::containsValue(int data)
{
	return containsValue(_root, data);
}

int binaryTree::size()
{
	return size(_root);
}

bool binaryTree::minValue(NODE * tree, int & data)
{
	int minimum;

	while (tree->left != nullptr)
	{
		tree = tree->left;
	}
	minimum = tree->data;

	if (minimum == data)
		return true;

	else
		return false;
}

bool binaryTree::minValue(int & data)
{
	return minValue(_root, data);
}

int binaryTree::maxDepth(NODE * tree)
{
	if (tree == nullptr)
		return 0;

	else
	{
		int leftDepth = maxDepth(tree->left);
		int rightDepth = maxDepth(tree->right);

		if (leftDepth > rightDepth)
			return leftDepth + 1;
		else
			return rightDepth + 1;
	}
}

int binaryTree::maxDepth()
{
	return maxDepth(_root);
}

void binaryTree::deepCopy(binaryTree & copyMe)
{
	copyTree(&_root, copyMe._root);
}

void binaryTree::copyTree(NODE ** dest, NODE * src)
{
	if (src == nullptr)
		return;

	(*dest) = createNode(src->data);

	if (src->left != nullptr)
	{
		copyTree(&(*dest)->left, src->left);
	}

	if (src->right != nullptr)
	{
		copyTree(&(*dest)->right, src->right);
	}
}

NODE * binaryTree::getRoot()
{
	return _root;
}

bool binaryTree::isEmpty()
{
	if (_root = nullptr)
		return true;

	else
		return false;
}

void binaryTree::operator=(binaryTree & right)
{
	this->clear();
	deepCopy(right);
}

int main()
{
	binaryTree tree1;
	binaryTree tree2;
	treePrinter print;
	

	int min1 = 3;
	int min2 = 5;

	
	tree1.insert(9);
	tree1.insert(5);
	tree1.insert(7);
	tree1.insert(12);
	tree1.insert(3);
	tree1.insert(16);
	tree1.insert(10);

	print.printPretty(tree1, cout);
	cout << "Tree 1" << endl;

	cout << endl << "Checking for a 3 in the tree: " << endl;
	if (tree1.containsValue(3))
	{
		cout << "The value is in the tree" << endl;
	}
	else
	{
		cout << "The value was not found in the tree" << endl;
	}

	cout << endl << "Checking for an 8 in the tree: " << endl;
	if (tree1.containsValue(8))
	{
		cout << "The value is in the tree" << endl;
	}
	else
	{
		cout << "The value was not found in the tree" << endl;
	}

	cout << endl << "The size of the tree is: " << tree1.size() << endl;

	cout << endl << "Checking to see if 3 is the minimum value in the tree: " << endl;
	if (tree1.minValue(min1))
	{
		cout << "The value is the minimum value in the tree" << endl;
	}
	else
	{
		cout << "The value is not the minimum value in the tree" << endl;
	}

	cout << endl << "Checking to see if 5 is the minimum value in the tree: " << endl;
	if (tree1.minValue(min2))
	{
		cout << "The value is the minimum value in the tree" << endl;
	}
	else
	{
		cout << "The value is not the minimum value in the tree" << endl;
	}

	cout << endl << "The max depth of the tree is: " << tree1.maxDepth() << endl;

	cout << endl << "Checking to see if the tree is empty: " << endl;
	if (tree1.isEmpty())
	{
		cout << "The tree is empty" << endl;
	}
	else
	{
		cout << "The tree is not empty" << endl;
	}

	tree2.insert(4);
	tree2.insert(5);
	tree2.insert(3);
	print.printPretty(tree2, cout);
	cout << "Tree 2" << endl;


	cout << endl << "Copying a tree 2 to tree 1" << endl;
	tree1 = tree2;

	print.printPretty(tree1, cout);
	cout << "New tree 1" << endl;

	return 0;
}

/*
ABSTRACT: Binary tree "pretty print" code.

CREDITS:
All of the print tree code is from:
http://www.ihas1337code.com/2010/09/how-to-pretty-print-binary-tree.html

The programmer for this code is 1337c0d3r

LIMITATIONS:
Unbalanced trees do not always print properly.

*/

#include <fstream>
#include <iostream>
#include <deque>
#include <iomanip>
#include <sstream>
#include <string>
using namespace std;

#include "treebase.h"

class treePrinter {
private:

	int maxHeight(NODE * root);
	string intToString(int val);
	void printBranches(int branchLen, int nodeSpaceLen, int startLen, int nodesInThisLevel,
		const deque<NODE *>& nodesQueue, ostream & out);
	void printNodes(int branchLen, int nodeSpaceLen, int startLen, int nodesInThisLevel,
		const deque<NODE *>& nodesQueue, ostream & out);
	void printLeaves(int indentSpace, int level, int nodesInThisLevel, const deque<NODE *>& nodesQueue, ostream & out);

	void printPretty(NODE *root, int level, int indentSpace, ostream& out);

public:

	treePrinter() {}
	~treePrinter() {}

	void printPretty(ITree & tree, int level, int indentSpace, ostream& out);
	void printPretty(ITree & tree, ostream& out);

}; // end treePrinter
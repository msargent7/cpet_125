// easy tree 

#include <iostream>
using namespace std;

typedef struct _NODE_ {
	int data;
	_NODE_* left;
	_NODE_* right;
}NODE, *PNODE;

bool isLeaf(PNODE node);
void  createTree(PNODE * tree) { *tree = NULL; }
PNODE createNode(int data);
PNODE insert(PNODE root, int data);
void  destroyTree(PNODE node);
void  printTree(PNODE root);
bool containsValue(NODE * tree, int data);
bool minValue(NODE * tree, int & data);
int size(NODE * tree);
void copyTree(NODE ** dest, NODE * src);
int maxDepth(NODE * tree);

int main()
{
	NODE * tree = nullptr;
	NODE * treeCopy = nullptr;

	bool contains;
	bool minimum;
	int min1 = 2;
	int min2 = 4;
	int sizeOf = 0;
	int depth = 0;

	createTree(&tree);

	tree = insert(tree, 5);
	tree = insert(tree, 3);
	tree = insert(tree, 8);
	tree = insert(tree, 2);
	tree = insert(tree, 4);
	tree = insert(tree, 6);
	tree = insert(tree, 9);

	printTree(tree);

	contains = containsValue(tree, 5);
	cout << contains << endl;
	contains = containsValue(tree, 15);
	cout << contains << endl;

	minimum = minValue(tree, min1);
	cout << minimum << endl;
	minimum = minValue(tree, min2);
	cout << minimum << endl;

	sizeOf = size(tree);
	cout << sizeOf << endl;

	copyTree(&treeCopy, tree);
	printTree(treeCopy);

	depth = maxDepth(tree);
	cout << depth << endl;

	cout << endl;

	destroyTree(tree);

	return 0;
}

// FUNCTION: isLeaf
// INPUTS: tree node, or NULL
// OUTPUT: true if the node is a leaf node, false otherwise
bool isLeaf(PNODE node)
{
	if (!node) return false;

	if (node->left == nullptr && node->right == nullptr) return true;

	return false;
}

PNODE createNode(int data)
{
	NODE * temp = new NODE;
	temp->left = nullptr;
	temp->right = nullptr;
	temp->data = data;

	return temp;
}

PNODE insert(PNODE root, int data)
{
	if (root == NULL) // empty tree
	{
		// creat the tree
		PNODE pTemp = createNode(data);

		// return the new root
		return pTemp;
	}
	else // not empty tree
	{
		if (data <= root->data)
		{
			// insert on left side
			root->left = insert(root->left, data);
		}
		else
		{
			// insert on right side
			root->right = insert(root->right, data);
		}
	}

	return root;
}

void destroyTree(PNODE node)
{
	if (node != NULL)
	{
		// destroy the left side of the tree
		destroyTree(node->left);

		// destroy the right side of the tree
		destroyTree(node->right);

		// output for debug purposes only
		cout << "deleting " << node->data << endl;

		// at this point, the tree has neither a left or a right
		delete node;
	}
}

void  printTree(PNODE root)
{
	if (root != NULL)
	{
		printTree(root->left);
		cout << root->data << endl;
		printTree(root->right);
	}
}

bool containsValue(NODE * tree, int data)
{

	if (tree == nullptr)
	{
		return false;
	}

	else
	{
		if (data == tree->data)
		{
			return true;
		}

		else if (data < tree->data)
		{
			return containsValue(tree->left, data);
		}

		else if (data > tree->data)
		{
			return containsValue(tree->right, data);
		}
	}
}

bool minValue(NODE * tree, int & data)
{
	int minimum;

	while (tree->left != nullptr)
	{
		tree = tree->left;
	}
	minimum = tree->data;

	if (minimum == data)
		return true;

	else
		return false;
}

int size(NODE * tree)
{
	int sizeOf = 0;

	if (tree == nullptr)
		return sizeOf;

	else
		sizeOf = size(tree->left) + size(tree->right) + 1;

	return sizeOf;
}

void copyTree(NODE ** dest, NODE * src)
{
	if (src == nullptr)
		return;

	(*dest) = createNode(src->data);
	
	if (src->left != nullptr)
	{
		copyTree(&(*dest)->left, src->left);
	}

	if (src->right != nullptr)
	{
		copyTree(&(*dest)->right, src->right);
	}
}

int maxDepth(NODE * tree)
{
	if (tree == nullptr)
		return 0;

	else
	{
		int leftDepth = maxDepth(tree->left);
		int rightDepth = maxDepth(tree->right);

		if (leftDepth > rightDepth)
			return leftDepth + 1;
		else
			return rightDepth + 1;
	}
}
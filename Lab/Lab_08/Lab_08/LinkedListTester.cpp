#include <iostream>
#include "linkedList.h"
using namespace std;

int main()
{
	linkedList list;

	//Populate the list by pushing values to the front
	list.push_front(1);
	list.push_front(2);
	list.push_front(3);
	list.push_front(4);
	list.push_front(5);
	list.push_front(6);

	//Populate the list by pushing values to the back
	list.push_back(100);
	list.push_back(200);
	list.push_back(300);
	list.push_back(400);
	list.push_back(500);
	list.push_back(600);

	cout << "Contents of list: " << endl << endl;
	
	for (NODE * itor = list.begin(); itor != NULL; itor = itor->next)
	{
		cout << itor->data << endl;
	}

	cout << endl << "Size is now: " << list.size() << endl;

	//Remove two values from the front of the list
	cout << endl << "Popping the front and back two items off the back of the list: " << endl << endl;

	list.pop_back();
	list.pop_back();

	//Remove two values from the back of the list
	list.pop_front();
	list.pop_front();

	for (NODE * itor = list.begin(); itor != NULL; itor = itor->next)
	{
		cout << itor->data << endl;
	}
	
	cout << endl << "Size is now: " << list.size() << endl;

	//Erase a node from the list
	cout << endl << "Erasing node at index 2: " << endl;
	list.erase(2);

	for (NODE * itor = list.begin(); itor != NULL; itor = itor->next)
	{
		cout << itor->data << endl;
	}

	cout << endl << "Size is now: " << list.size() << endl;

	//Erase a node from the list
	cout << endl << "Erasing node at index 5: " << endl;
	list.erase(5);

	for (NODE * itor = list.begin(); itor != NULL; itor = itor->next)
	{
		cout << itor->data << endl;
	}

	cout << endl << "Size is now: " << list.size() << endl;
	
	//Erase a range of nodes from the list
	cout << endl << "Erasing nodes from 1-3: " << endl;
	list.erase(1, 3);

	for (NODE * itor = list.begin(); itor != NULL; itor = itor->next)
	{
		cout << itor->data << endl;
	}

	cout << endl << "Size is now: " << list.size() << endl;
	
	//Reverse the list
	cout << endl << "Reversing the list: " << endl;

	list.revese();
		
	cout << endl << "Size is now: " << list.size() << endl;

	//Clear the list
	cout << endl << "Clearing the list..." << endl << endl;

	list.clear();

	cout << "The head is now: " << list.begin() << endl;
	cout << "The tail is now: " << list.end() << endl;
	cout << "Size: " << list.size() << endl << endl;

	return 0;
}
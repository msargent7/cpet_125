/*

Linked List Sample.  Study this code carefully.

Add the functions in the ADD THESE FUNCTIONS and the
REWRITE / ADD RECURSIVE FUNCTIONS sections below.
Add function descriptions to all the functions in this code.
Demonstrate the results to the instructor.

*/

#include <iostream>
#include "linkedList.h"
using namespace std;

/*
FUNCTION:		createNode
PRECONDITION:	A value to store in the list.
POSTCONDITION:	Returns a pointer to a node containing the value.
*/
NODE * createNode(int value)
{
	NODE * temp = new NODE;
	temp->data = value;
	temp->next = 0;
	return temp;
}


linkedList::linkedList()
{
	_head = _tail = nullptr;
	_size = 0;
}

linkedList::~linkedList()
{
	delete _head;
}

NODE * linkedList::begin()
{
	return _head;
}

NODE * linkedList::end()
{
	return _tail;
}


void linkedList::insert(int position, int value)
{
	NODE * temp = createNode(value);
	int count = 0;

	if (position == 0)
	{
		temp->next = _head;
		_head->prev = temp;
		_head = temp;
	}

	else
	{
		for (NODE * it = _head; it != nullptr; it = it->next)
		{
			if (count == position)
			{
				temp->next = it;
				temp->prev = it->prev;
				it->prev = temp;
			}
			count++;
		}
	}
	_size++;
}

void linkedList::push_front(int value)
{
	NODE * temp = createNode(value);
	
	if (_head == nullptr)
	{
		_head = _tail = temp;
	}

	else
	{
		temp->next = _head;
		_head->prev = temp;
		_head = temp;
	}
	_size++;
}

void linkedList::push_back(int value)
{
	NODE * temp = createNode(value);

	if (_head == nullptr)
	{
		_head = _tail = nullptr;
	}

	else
	{
		temp->prev = _tail;
		_tail->next = temp;
		_tail = temp;
	}
	_size++;
}

int linkedList::front()
{
	return _head->data;
}

int linkedList::back()
{
	return _tail->data;
}

void linkedList::pop_front()
{
	if (_head == nullptr && _tail == nullptr)
	{
		cout << "There's nothing to remove, the list is empty!" << endl;
	}

	else if (_head->next == nullptr)
	{
		delete _tail;
		_tail = _head = nullptr;
	}

	else
	{
		NODE * temp = _head;
		_head = _head->next;
		_head->prev = nullptr;
		delete temp;
	}
	_size--;
}

void linkedList::pop_back()
{
	if (_head == nullptr && _tail == nullptr)
	{
		cout << "There's nothing to remove, the list is empty!" << endl;
	}

	else if (_head == _tail)
	{
		delete _tail;
		_tail = _head = nullptr;
	}

	else
	{
		NODE * last = _tail;
		_tail = _tail->prev;
		delete last;
		_tail->next = nullptr;
	}
	_size--;
}

void linkedList::erase(int position)
{
	int count = 0;

	if (position == 0)
	{
		_head = _head->next;
		_head->prev = nullptr;
	}

	for (NODE * it = _head; it != nullptr; it = it->next)
	{
		if (count == position)
		{
			it->prev->next = it->next;
			it->next->prev = it->prev;
		}
		count++;
	}
	_size--;
}

void linkedList::erase(int first, int last)
{
	int count = 0;

	for (NODE * it = _head; it != nullptr; it = it->next)
	{
		if (count == last)
		{
			for (count; count >= first; count--)
			{
				erase(count);
			}
		}
		count++;
	}
}

void linkedList::clear()
{
	NODE * temp = _head;

	while (_head != nullptr)
	{
		pop_front();
	}
	_size = 0;
}

void linkedList::revese()
{

	NODE * current;
	NODE * next;
	NODE * prev = NULL;
	NODE * revList;
	current = _head;

	while (current != NULL)
	{
		next = current->next;
		prev = current->prev;
		current->next = prev;
		prev = current;
		current = next;
	}

	revList = prev;

	for (NODE * itor = revList; itor != NULL; itor = itor->next)
	{
		cout << itor->data << endl;
	}
}
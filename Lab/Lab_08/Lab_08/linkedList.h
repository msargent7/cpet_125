/*

Here is one possible implementation:

head or front                         tail or back

100
next ->		200
NULL <-prev		next ->  300
<-prev     next ->  400
<-prev     next -> NULL
<-prev

*/

#ifndef _LINKED_LIST_H_
#define _LINKED_LIST_H_


#include <iostream>
using namespace std;

struct NODE {

	int data;
	NODE * next;
	NODE * prev;

};

class linkedList {
private:

	NODE * _head;
	NODE * _tail;
	int    _size;

	// additional private functions here

public:

	linkedList();  // default constructor
	~linkedList(); // be sure to deallocate all memory

				   // general functions
	int size() { return _size; }

	// our list does not use iterators, so we will iterate ourselves
	NODE * begin(); // returns a pointer to the first node that contains data. 
	NODE * end();

					// functions to add elements
	void insert(int position, int value);
	void push_front(int value); // insert a copy of value at the head
	void push_back(int value);  // insert a copy of value at the tail

								// function to view elements
	int front(); // view the data at the head
	int back();  // view the data at the tail

				 // functions to remove elements
	void pop_front();  // remove and delete the node at the head
	void pop_back();   // remove and delete the node at the tail

					   // functions to delete elements
	void erase(int position);        // removes the element at postion
	void erase(int first, int last); // removes a range of elements
	void clear();                    // removes all elements from the list

									 // functions to manipulate the list
	void revese(); // reverses the contents of the list

				   // output
	friend ostream & operator <<(ostream & out, linkedList & list);

};

#endif
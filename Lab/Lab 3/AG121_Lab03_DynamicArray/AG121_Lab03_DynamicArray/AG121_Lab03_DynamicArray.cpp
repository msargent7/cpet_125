/*
FILE: <the name of the file>
PROGRAMMER: <your name>
DATE: <the date the project is submitted>
PROJECT: <the name of the project >
CREDITS: <A list of web sites, books, and programmers used to create the code.  You do not need to list any text book used in this course or the instructors name. >
LIMITATIONS: <Any limitations your program may have.  For example, Your  program might fail if the user inputs a number instead of a string.>
*/



#include <iostream>
using namespace std;

#include "easyArray.h"

void testArray(); 
void testCopyConstructor(); 
void testAssignmentOperator(); 
void testResize(); 
void testReverse(); 
void testInsertAt(); 

int main()
{
	testArray(); 
	testCopyConstructor(); 
	testAssignmentOperator(); 
	testResize(); 
	testReverse(); 
	testInsertAt(); 

	return 0; 
}

void testArray()
{
	easyArray a(5); 

	for(int i = 0; i < a.size(); i++)
	{
		a[i] = i + 100; 
	}

	// overloaded << operator
	cout << "TEST ARRAY: " << a << endl << endl; 
}

void testCopyConstructor() 
{
	easyArray a(5); 

	for(int i = 0; i < a.size(); i++)
	{
		a[i] = i + 100; 
	}

	// initialization: copy constructor
	easyArray b = a; 

	// overloaded << operator
	cout << "TEST COPY CONSTRUCTOR: " << b << endl << endl; 
}

void testAssignmentOperator()
{
	easyArray a(5); 

	for(int i = 0; i < a.size(); i++)
	{
		a[i] = i + 100; 
	}

	// default constructor call
	easyArray b; 

	// assignment
	b = a; 

	// overloaded << operator
	cout << "TEST ASSIGNMENT: " << b << endl << endl; 
}

void testResize()
{
	easyArray a(5); 

	for(int i = 0; i < a.size(); i++)
	{
		a[i] = i + 100; 
	}

	a.resize(10); 

	a[9] = 99; 

	// overloaded << operator
	cout << "TEST RESIZE: " << a << endl << endl; 
}

void testReverse() 
{
	easyArray a(5); 

	for(int i = 0; i < a.size(); i++)
	{
		a[i] = i + 100; 
	}

	// overloaded << operator
	cout << "TEST REVERSE BEFORE: " << a << endl; 
	a.reverse(); 
	cout << "TEST REVERSE AFTER: " << a << endl << endl; 

}

void testInsertAt() 
{
	easyArray a(5); 

	for(int i = 0; i < a.size(); i++)
	{
		a[i] = i + 100; 
	}

	// overloaded << operator
	cout << "TEST INSERT AT BEFORE: " << a << endl; 

	a.insertAt(7000, 2); 

	cout << "TEST INSERT AT AFTER: " << a << endl << endl; 

}

/*
PROOF:

TEST ARRAY: 100, 101, 102, 103, 104

TEST COPY CONSTRUCTOR: 100, 101, 102, 103, 104

TEST ASSIGNMENT: 100, 101, 102, 103, 104

TEST RESIZE: 100, 101, 102, 103, 104

TEST REVERSE BEFORE: 100, 101, 102, 103, 104
TEST REVERSE AFTER: 104, 103, 102, 101, 100

TEST INSERT AT BEFORE: 100, 101, 102, 103, 104
TEST INSERT AT AFTER: 100, 101, 7000, 102, 103, 104

Press any key to continue . . .

*/

/*
Proof

TEST ARRAY: 100, 101, 102, 103, 104

TEST COPY CONSTRUCTOR: 100, 101, 102, 103, 104

TEST ASSIGNMENT: 100, 101, 102, 103, 104

TEST RESIZE: 100, 101, 102, 103, 104

TEST REVERSE BEFORE: 100, 101, 102, 103, 104
TEST REVERSE AFTER: 104, 103, 102, 101, 100

TEST INSERT AT BEFORE: 100, 101, 102, 103, 104
TEST INSERT AT AFTER: 100, 101, 7000, 102, 103

Press any key to continue . . .

*/

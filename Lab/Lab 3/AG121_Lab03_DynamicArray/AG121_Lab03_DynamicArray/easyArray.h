#ifndef _SIMPLE_ARRAY_H_
#define _SIMPLE_ARRAY_H_

#include <iostream>
using namespace std; 

class easyArray{
private:

	int * _theArray; 
	int   _size; 

	void copy(easyArray & right); 

public:

	/*
	 * Function: default constructor
	 * Usage:    easyArray a; 
	 * --------------------------------------
	 * This constructor sets the array to NULL and size to zero. 
	 */
	easyArray(); 

	/*
	 * Function: overloaded constructor
	 * Usage:    easyArray a(10); 
	 * --------------------------------------
	 * This constructor allocates size elements and initializes 
	 * each element to zero 
	 */
	easyArray(unsigned int size); 


	/*
	 * Function: copy constructor
	 * Usage:    easyArray a(10); 
	 * --------------------------------------
	 * This constructor allocates size elements and initializes 
	 * each element to zero 
	 */
	easyArray(easyArray & right); 

	/*
	 * Function: destructor
	 * --------------------------------------
	 * called when the object is destroyed
	 */
	~easyArray(); 

	/*
	 * Function: destroy
	 * Usage:    a.destroy(); 
	 * --------------------------------------
	 * deallocates any allocated memory and sets the size to zero. 
	 */
	void destroy(); 

	/*
	 * Function: resize 
	 * Usage:    a.resize(); 
	 * --------------------------------------
	 * creates a new array larger than the previous array.  If the previous
	 * array is larger than the new size, the array is not resized. 
	 */
	void resize(unsigned int newSize); 

	/*
	 * Function: reverse
	 * Usage:    a.reverse(); 
	 * --------------------------------------
	 * reverses the values in the array 
	 */
	void reverse(); 

	/*
	 * Function: size
	 * Usage:    for(int i = 0; i < a.size(); i++)
	 *				cout << a[i] << endl; 
	 * --------------------------------------
	 * returns the number of elements in the array 
	 */
	int size() { return _size; }

		/*
	 * Function: fill
	 * Usage:    a.fill(-1); 
	 * --------------------------------------
	 * sets every element in array to value
	 */
	void fill(int value); 

	/*
	 * Function: insertAt
	 * Usage:    a.insertAt(4000, 0); 
	 * --------------------------------------
	 * sets the element at index to the value.  All values from
	 * index to size() - 1 are 'moved over'. 
	 */
	void insertAt(int value, unsigned int index); 

		/*
	 * Function: operator[]
	 * Usage:    a[9] = 42
	 * --------------------------------------
	 * returns a reference to element at index
	 */
	int & operator[](int index); 

	/*
	 * Function: operator=
	 * Usage:    a = b 
	 * --------------------------------------
	 * sets the state of a to match b 
	 */
	easyArray & operator=(easyArray & right); 

	/*
	 * Function: operator <<
	 * Usage:    cout << a;  
	 * --------------------------------------
	 * outputs the array to the ostream object
	 */
	friend ostream & operator<<(ostream & out, easyArray & right); 
}; 

#endif 

#include "easyArray.h" // you will need the header file so we have the class definition


easyArray::easyArray()
{
	_size = 0;     // no elements in the array yet
	_theArray = 0; // points to nothing
}

easyArray::easyArray(unsigned int size)
{
	_size = size;
	_theArray = new int[_size];

	for (int i = 0; i < size; i++)
	{
		_theArray[i] = 0;
	}
}

easyArray::easyArray(easyArray & right)
{
	// no code for you to put in this function

	copy(right);
}

void easyArray::copy(easyArray & right)
{
	_size = right.size();

	_theArray = new int[right.size()];

	for (int i = 0; i < _size; i++)
	{
		_theArray[i] = right[i];
	}
}

easyArray::~easyArray()
{
	// delete the array if it exists, or there will be a memory leak...
	delete[] _theArray;
}

void easyArray::destroy()
{
	_size = 0;
	if (_theArray) delete[] _theArray;
}

void easyArray::resize(unsigned int newSize)
{
	// create a new dynamic array
	int *newArray;
	newArray = new int[newSize];

	// init the new array to zero
	for (int i = 0; i < newSize; i++)
	{
		newArray[i] = 0;
	}

	// copy the old array into the new array.  You might
	// want to be sure newSize <= _size
	for (int i = 0; i < _size; i++)
	{
		newArray[i] = _theArray[i];
	}

	// delete the old array (_theArray)
	delete[] _theArray;

	// point _theArray at the new array
	_theArray = newArray;
}

void easyArray::reverse()
{
	// your code goes here.


	int temp;

	for (int i = 0; i < _size / 2; i++)
	{
		temp = _theArray[_size - i - 1];
		_theArray[_size - i - 1] = _theArray[i];
		_theArray[i] = temp;
	}
}

void easyArray::insertAt(int value, unsigned int index)
{
	// make room for the value by moving values over.  OK to have values
	// 'drop off' the end of the array.  OK to resize the array.

	int *insertArray;
	insertArray = new int[_size + 1];
	

	// insert value at index
	for (int i = 0; i < _size; i++)
	{
		if (i < index)
		{
			insertArray[i] = _theArray[i];
		}

		if (i == index)
		{
			insertArray[i] = value;
		}

		if (i > index)
		{
			insertArray[i] = _theArray[i - 1];
		}
	}

	// delete the old array (_theArray)
	delete[] _theArray;

	// point _theArray at the new array
	_theArray = insertArray;
}

void easyArray::fill(int value)
{
	// no code to place here; the function is complete

	for (int i = 0; i < size(); i++)
	{
		_theArray[i] = value;
	}
}

int & easyArray::operator[](int index)
{
	// no code to place here; the function is complete for now

	// do not worry about index out of range or
	// NULL reference exceptions (errors) at this point

	return _theArray[index];
}

easyArray & easyArray::operator=(easyArray & right)
{
	// no code for you to put in this function

	if (this == &right) // do not copy yourself
	{
		return *this;
	}

	if (_theArray != NULL)
	{
		delete[] _theArray;
	}

	copy(right);
}

// nothing to do here:
ostream & operator<<(ostream & out, easyArray & right)
{
	// this function is a friend of the class, so the function has access
	// to the classes private variables

	for (int i = 0; i < right._size; i++)
	{
		cout << right._theArray[i];

		if (i < right._size - 1)
		{
			cout << ", ";
		}
	}

	// always return the stream object
	return out;
}

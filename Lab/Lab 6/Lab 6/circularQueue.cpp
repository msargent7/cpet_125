#include <iostream>
#include "circularQueue.h"
using namespace std;

circularQueue::circularQueue()
{
	m_front = m_back = 0;

	for (int i = 0; i < MAX_QUEUE; i++)
	{
		m_theQueue[i] = 0;
	}
}

circularQueue::circularQueue(const circularQueue & right)
{

}

bool circularQueue::enqueue(int data)
{
	
	if (m_front == (m_back + 1) % MAX_QUEUE)
	{
		return false;
	}

	else
	{
		m_theQueue[m_back] = data;
		m_back = (m_back + 1) % MAX_QUEUE;
		return true;
	}
}

bool circularQueue::dequeue(int & out)
{
	if (abs(m_front == m_back))
	{
		return false;
	}

	else
	{
		int temp;
		temp = m_theQueue[m_front];
		m_front = (m_front + 1) % MAX_QUEUE;
		return true;
	}
}

int circularQueue::size()
{
	int count = 0;

	for (int i = m_front; i != m_back + 1; i++)
	{
		if (i = MAX_QUEUE - 1)
		{
			i = 0;
		}
		count++;
	}

	return count;
}

void circularQueue::print()
{
	for (int i = 0; i < MAX_QUEUE; i++)
	{
		cout << m_theQueue[i] << ", ";
	}
	cout << endl << endl;
}
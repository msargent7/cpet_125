#ifndef _CIRCULARQUEUE_H
#define _CIRCULARQUEUE_H

const int MAX_QUEUE = 5;

class circularQueue {
private:

	int m_theQueue[MAX_QUEUE];
	int m_front;  //dequeue
	int m_back;   //enque
	int count = 0;

public:
	circularQueue();
	circularQueue(const circularQueue & right);
	bool enqueue(int value);
	bool dequeue(int & out);
	void print();
	int size();

};


#endif
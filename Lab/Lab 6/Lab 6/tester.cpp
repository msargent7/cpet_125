#include <iostream>
#include <conio.h>
#include "circularQueue.h"
using namespace std;

enum CMD {
	ENQUEUE = '1',
	DEQUEUE,
	PRINT,
	HELP,
	END
};

void menu();
void print(circularQueue q);

int main()
{
	circularQueue q;
	bool loop = true;
	int input = 0, value = 0, q_out = 0;

	cout << "This program demonstrates a circular queue." << endl;

	menu();

	while (loop) 
	{
		cout << "Input: ";
		input = _getche();
		cout << endl;

		switch (input) 
		{
		case ENQUEUE:

			if (q.enqueue(value))
			{ 
				cout << "value " << value << " inserted into queue." << endl;
				value++;
			}
			else
			{
				cout << "queue is full." << endl;
			}
			break;

		case DEQUEUE:
			if (q.dequeue(q_out))
			{
				cout << "value removed from queue: " << q_out << endl;
			}
			else 
			{ 
				cout << "queue is empty." << endl;
			}
			break;

		case PRINT:    
			q.print();
			break;    
		
		case HELP:    
			menu();    
			break;    
		
		case END:    
			loop = false;     
			break;    
		
		default:    
			cout << "invalid choice." << endl;    
			menu();

		}// end switch  

	}// end loop  

	system("pause");

	return 0;
}

void menu()
{
	cout << "1. Enqueue" << endl;
	cout << "2. Dequeue" << endl;
	cout << "3. Print" << endl;
	cout << "4. Help" << endl;
	cout << "5. End" << endl;
}

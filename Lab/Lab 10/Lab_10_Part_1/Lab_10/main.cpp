#include <iostream>
#include <queue>
#include <list>
#include <string>
using namespace std;

enum MyEnum
{
	ENQUEUE,
	DEQUEUE,
	CLEAR,
	EXIT
};

void menu();

typedef queue <string> STRINGQUEUE;

int main()
{

	STRINGQUEUE q;
	string input;
	int choice;

	while (1)
	{

		menu();
		cout << "enter a choice: ";
		cin >> choice;		

		switch (choice)
		{
		case ENQUEUE:
			cin.ignore();
			cout << "enter a string: ";
			getline(cin, input);
			q.push(input);
			break;

		case DEQUEUE:
			if (!q.empty())
			{
				cout << q.front() << endl;
				q.pop();
			}
			else
				cout << "The queue is empty" << endl;
			break;

		case CLEAR:
			while (!q.empty())
			{
				q.pop();
			}
			break;

		case EXIT:
			return 0;
			break;

		default:
			cout << "Invalid choice" << endl;
			break;
		}
	}
}

void menu()
{
	cout << ENQUEUE << ". Enqueue" << endl;
	cout << DEQUEUE << ". Dequeue" << endl;
	cout << CLEAR << ". Clear" << endl;
	cout << EXIT << ". Exit" << endl;
}
// TODO: Add your header here

#include <iostream>
#include <list>
#include <string>

using namespace std;

const int HASH_TABLE_SIZE = 10;

typedef list<string> str_list;
typedef list<string>::iterator str_list_itor;

// http://digital.cs.usu.edu/~allanv/cs2200/Sahni7a/Sahni7a.html

unsigned _hash(string const& key, unsigned tableSize);

void printList(str_list);

int main()
{
	str_list hash_table[HASH_TABLE_SIZE];
	string input;
	unsigned key;

	for (int i = 0; i < 12; i++) // only input 12 strings into the hash table
	{
		cout << "enter a string: ";
		getline(cin, input);

		// get the index (key) into the array using the has function
		key = _hash(input, HASH_TABLE_SIZE);

		// key indexes into the array.  push_back is a function of STL list, in this
		// case the list at index key in the array of STL lists.
		hash_table[key].push_back(input);
	}

	// TODO: Print the hash table
	for (int i = 0; i < 10; i++)
	{
		printList(hash_table[i]);
	}
	cout << endl;

	return 0;
}

unsigned _hash(string const& key, unsigned tableSize)
{
	unsigned hashVal = 0;

	for (int x = 0; x < key.length(); ++x)
	{
		hashVal ^= (hashVal << 5) +
			(hashVal >> 2) +
			key[x];
	}

	return hashVal % tableSize;
}

void printList(str_list hash_table)
{
	list<string>::const_iterator i;
	for (i = hash_table.begin(); i != hash_table.end(); i++)
	{
		cout << *i << " ";
	}
}

/*
FILE: AG121_Lab01_Dice_OOP.cpp
PROGRAMMER: <your name>
DATE: <the date the project is submitted>
PROJECT: AG121_Lab01_Dice_OOP
CREDITS: <A list of web sites, books, and programmers used to create the code.  
          You do not need to list any text book used in this course or the instructors name. >
LIMITATIONS: <Any limitations your program may have.  For example, Your  program might fail if the user inputs a number instead of a string.>
*/


#include <iostream>
#include <ctime>
using namespace std;

class dice{
private:

	int _nSides; 
	int _nDice; 
	int _value; 

public:
	
	dice()
	{
		srand(time(0)); 
		_value = 0; 
		
		// perform rest of initialization here
		_nDice = 1;
		_nSides = 6;
	}

	dice(int nDice)
	{
		srand(time(0)); 
		_value = 0; 

		// perform rest of initialization here
		_nDice = nDice;
		_nSides = 6;
	}

	dice(int nDice, int nSides)
	{
		srand(time(0)); 
		_value = 0; 

		// perform rest of initialization here
		_nDice = nDice;
		_nSides = nSides;
	}

	void roll()
	{
		// your code goes here 
		int total = 0;

		for (int i = 0; i < _nDice; i++)
		{
			total += (rand() % _nSides) + 1;
		}
		_value = total;
	}

	int value()
	{
		return _value;
	}

}; 

void testDie(); 

int main()
{

	// no need to init random number generator, it's done in the constructor

	dice die; // one die, 6 sides
	dice twoDice(2); // two dice, 6 sides each
	dice twoMultiSided(2, 10); // two dice, ten sides each 

	cout << "This program tests a dice class." << endl; 

	testDie(); 

	die.roll(); 
	cout << "The result of one dice roll is: " << die.value() << endl; 

	twoDice.roll();  
	cout << "The result of two dice rolls is: " << twoDice.value() << endl; 

	twoMultiSided.roll(); 
	cout << "The result of two dice rolls with 10 sides is: " << twoMultiSided.value() << endl; 

	system("pause"); 

	return 0; 
}

void testDie()
{
	dice die; 

	int valueCount[7] = { 0 }; // results 1 - 6, index zero not used
	int i; 

	for(i = 0; i < 10000; i++)
	{
		die.roll(); // get the dice roll value, 1 - 6

		// the state of the object is the value of the last roll
		if(die.value() > 0 && die.value() < 7)
		{
			valueCount[die.value()] = valueCount[die.value()] + 1; // add one to the count of rolls
		}
		else
		{
			cout << "Error.  Roll value out of range at " << die.value() << endl; 
			return; // nothing else to do in function
		}
	}

	for(i = 1; i < 7; i++)
	{
		cout << "value " << i << " was returned " << valueCount[i] << " times." << endl; 
	}
}

/* Proof

This program tests a dice class.
value 1 was returned 1629 times.
value 2 was returned 1661 times.
value 3 was returned 1646 times.
value 4 was returned 1676 times.
value 5 was returned 1662 times.
value 6 was returned 1726 times.
The result of one dice roll is: 4
The result of two dice rolls is: 9
The result of two dice rolls with 10 sides is: 19
Press any key to continue . . .

*/
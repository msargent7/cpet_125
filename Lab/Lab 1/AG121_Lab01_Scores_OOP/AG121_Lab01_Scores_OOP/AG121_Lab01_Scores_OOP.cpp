/*
FILE: AG121_Lab01_Scores_OOP.cpp
PROGRAMMER: <your name>
DATE: <the date the project is submitted>
PROJECT: AG121_Lab01_Dice_OOP
CREDITS: <A list of web sites, books, and programmers used to create the code.  
          You do not need to list any text book used in this course or the instructors name. >
LIMITATIONS: <Any limitations your program may have.  For example, Your  program might fail if the user inputs a number instead of a string.>
*/

#include <iostream>
#include <fstream> 
#include <string>
using namespace std; 

const char * FILE_NAME = "scores.txt"; 
const int MAX_SCORES = 100; 
const int STR_MAX = 16; 

class gameScores{
private:

	int _theScores[MAX_SCORES]; 
	char _thePlayers[MAX_SCORES][STR_MAX];  
	int _count; 

public:

	gameScores()
	{
		_count = 0; 

		for(int i = 0; i < MAX_SCORES; i++)
		{
			_theScores[i] = 0; 
		}
	}

	~gameScores()
	{
		// no code to put here
	}

	bool getScores(const char * path)
	{
		fstream file(path); 

		if(file.is_open() == false)
		{
			cout << "failed to open file." << endl; 
			return 0; // end program
		}
		_count = 0;
		while(file.good())
		{
			file >> _thePlayers[_count] >> _theScores[_count]; 
			_count++; 
		}
		return true;  
	}

	/*
	* Implementation notes: average()
	* -------------------------------------
	* This function returns the average of all the scores in the file
	*/ 
	double average()
	{ 
		int total = 0;
		int average;

		for(int i = 0; i < MAX_SCORES; i++)
		{
			total += _theScores[i];
		}

		average = total / _count;

		return total;
	}

	/*
	* Implementation notes: count()
	* -------------------------------------
	* This function returns the number of scores in the file
	*/ 
	int count()
	{
		return _count;
	}

	/*
	* Implementation notes: total()
	* -------------------------------------
	* This function returns the total of all the scores in the file
	*/ 
	int total()
	{
		int total = 0;

		for (int i = 0; i < MAX_SCORES; i++)
		{
			total += _theScores[i];
		}

		return total; // stub
	}


	/*
	* Implementation notes: highest()
	* -------------------------------------
	* This function returns the index of the highest score
	*/ 
	int highest()
	{
		int highest = _theScores[0];
		int highestIndex = 0;

		for (int i = 0; i < _count; i++)
		{
			if (_theScores[i] > highest)
			{
				highest = _theScores[i];
				highestIndex = i;
			}
		}

		return highestIndex; 
	}

	/*
	* Implementation notes: lowest()
	* -------------------------------------
	* This function returns the index of the lowest score in the file
	*/ 
	int lowest()
	{
		int lowest = _theScores[0];
		int lowestIndex = 0;

		for (int i = 0; i < _count; i++)
		{
			if (_theScores[i] < lowest)
			{
				lowest = _theScores[i];
				lowestIndex = i;
			}
		}

		return lowestIndex;  
	}

	/*
	* Implementation notes: getScoreAt()
	* -------------------------------------
	* This function returns the value stored at index in _theScores.
	* There is no provision for index out of bounds.  
	*/ 
	int getScoreAt(int index)
	{
		return _theScores[index]; 
	}

	/*
	* Implementation notes: getPlayerAt()
	* -------------------------------------
	* This function returns the player name at index in _thePlayers  
	There is no provision for index out of bounds. 
	*/ 
	char * getPlayerAt(int index)
	{
		return _thePlayers[index]; 
	}
}; // end class scores

/*
* Function: printScores
* Usage: 
*	scores s; 
*	s.getScores("scores.txt"); 
*	printScores(s); 
* ---------------------------------
* This function prints to the console all the scores stores in the scores object.
*/
void printScores(gameScores & a); 

int main()
{

	gameScores scores; 
	int index; 
	bool result; 

	cout << "This program tests the game scores class." << endl << endl; 

	result = scores.getScores(FILE_NAME); 

	if(result == false)
	{
		cout << "failed to get scores." << endl; 
		system("pause"); 

		return 0; 
	}

	cout << "Scores: " << endl; 
	printScores(scores);

	cout << endl; 

	index = scores.highest(); 
	cout << "High Score: "; 
	cout << "\t" << scores.getPlayerAt(index) << "\t" << scores.getScoreAt(index) << endl; 

	index = scores.lowest(); 
	cout << "Low Score: "; 
	cout << "\t" << scores.getPlayerAt(index) << "\t" << scores.getScoreAt(index) << endl << endl; 

	cout << "Total:   " << scores.total() << endl; 
	cout << "Average: " << scores.average() << endl; 

	cout << endl; 

	system("pause"); 

	return 0; 
}

void printScores(gameScores & a)
{
	for(int i = 0; i < a.count(); i++)
	{
		int n = a.getScoreAt(i); 
		cout << n << endl; 
		//cout << a.getPlayerAt(i) << "\t" << a.getScoreAt(i) << endl; 
	}
}

/* Proof

This program tests the game scores class.

Scores:
100
89
77
30
88
90
50
3
44
65

High Score:     Bill    100
Low Score:      Jon     3

Total:   636
Average: 636

Press any key to continue . . .
/*

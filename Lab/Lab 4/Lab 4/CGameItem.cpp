/*
* CGameItem.cpp
* ---------------------------------------------
* Implementation for items
*
*/

#include "CGameItem.h"

// CSpell
CSpell::CSpell()
{
	_value = 5;
	_description = "Spell";
}

CSpell::CSpell(CSpell & right)
{
	_value = right._value;
	_description = right._description;
}

void CSpell::use()
{
	cout << "Expecto patronus!" << endl;
}


// CPotion
CPotion::CPotion()
{
	_value = 2;
	_description = "Potion";
}

CPotion::CPotion(CPotion  & right)
{
	_value = right._value;
	_description = right._description;
}

void CPotion::use()
{
	cout << "Glug glug glug" << endl;
}

// CWeapon
CSword::CSword()
{
	_value = 7;
	_description = "Sword";
}

CSword::CSword(CSword & right)
{
	_value = right._value;
	_description = right._description;
}

void CSword::use()
{
	cout << "Hiyah!";
}

// CShield
CShield::CShield()
{
	_value = 3;
	_description = "Shield";
}

CShield::CShield(CShield & right)
{
	_value = right._value;
	_description = right._description;
}

void CShield::use()
{
	cout << "Take cover!";
}

// CBomb
CBomb::CBomb()
{
	_value = 3;
	_description = "Bomb";
}

CBomb::CBomb(CBomb & right)
{
	_value = right._value;
	_description = right._description;
}

void CBomb::use()
{
	cout << "Ka-Boom!";
}

// CBow
CBow::CBow()
{
	_value = 3;
	_description = "Bow and arrow";
}

CBow::CBow(CBow & right)
{
	_value = right._value;
	_description = right._description;
}

void CBow::use()
{
	cout << "Thwwp thwwp!";
}

// CTorch
CTorch::CTorch()
{
	_value = 3;
	_description = "Torch";
}

CTorch::CTorch(CTorch & right)
{
	_value = right._value;
	_description = right._description;
}

void CTorch::use()
{
	cout << "Crackle crackle crackle";
}







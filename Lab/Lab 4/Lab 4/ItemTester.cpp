/*
* ItemTester.cpp
* ---------------------------------------------------
* Tester for CGameItem derived classes.
*
* The student should modify the following functions:
*
*	getItemToAdd
*	add
*	remove
*
* The student should also modify the enum
*
* Suggest you start by getting the add function to work and test.
* Next, get the remove function to work.
* Next, add your own classes.
* Test your classes by adding to the array of pointers.
*
* Challenge yourself:	Remove an item if it used up - create your own rules for this
*						Let the player encounter obstacles and require items
*						Further limit number of items player can have so it is harder to complete the game
*						Create a class the stores the array of pointers (Do not use the STL; it does not work well with pointers)
*
*/

/*
FILE: ItemTester.cpp
PROGRAMMER: <your name>
DATE: <the date the project is submitted>
PROJECT: AG121_Lab04
CREDITS: <A list of web sites, books, and programmers used to create the code.
You do not need to list any text book used in this course or the instructors name. >
LIMITATIONS: <Any limitations your program may have.  For example, Your  program might fail if the user inputs a number instead of a string.>
*/

#include <iostream>
#include <string>
using namespace std;

#include <conio.h>

#include "CGameItem.h"

// TODO: Add choices to the enum
enum INPUT_CHOICES {
	SPELL,
	POTION,
	SWORD,
	SHIELD,
	BOMB,
	BOW,
	TORCH

	// TODO: Add to enum
};

const int MAX_ITEMS = 5;


/*
* Function: getItemToAdd
* Usage: CGameItem * w = getItemToAdd();
* -----------------------------------
* If successful, this function returns a valid pointer
* to an object derived from type CGameItem.  If the  function
* fails, the function returns NULL.
*/
CGameItem * getItemToAdd();

/*
* Function: getNumber
* Usage: bool result = getNumber(number);
* -----------------------------------
* This function gets a number input by the user.  If the user
* inputs a number, the function returns true and sets number
* the the value input by the user.  If the function returns false,
* the value of number is not changed.
*
*/
bool getNumber(int & number);

/*
* Function: add
* Usage: add(weapons, w, MAX, count)
* -----------------------------------
* This function adds a valid pointer to an array of CWeapon
* pointers.  If successful, the function returns true and the
* value of count is the number of weapons in the pointer array.
* If the function fails, it returns false and the value of count
* is unchanged.
*/
bool add(CGameItem ** items, CGameItem * itemToAdd, int max, int & count);

/*
* Function: remove
* Usage: remove(items, count, index)
* -----------------------------------
* If successful, this function deletes the item at index and
* sets count to the new count of items in the array.  Otherwise,
* the function returns false and count is unchanged.
*/
bool remove(CGameItem ** items, int & count, int index);

/*
* Function: showItems
* Usage: showItems(items, count)
* -----------------------------------
* Prints a list of item descriptions to the console.
*/
void showItems(CGameItem ** items, int count);

/*
* Function: mainMenu
* Usage: mainMenu()
* -----------------------------------
* Prints the main menu to the console.
*/
void mainMenu();

/*
* Function: itemStore
* Usage: itemStore()
* -----------------------------------
* Allows the user to add drop or list items
*/
void itemStore(CGameItem ** items, CGameItem * itemToAdd, int max, int & count);

/*
* Function: goFight
* Usage: goFight()
* -----------------------------------
* Brings the user to the fight selection menu
*/
void goFight(CGameItem ** items, int count);

/*
* Function: dragonFight
* Usage: dragonFight()
* -----------------------------------
* Allows the user to fight the dragon
*/
void dragonFight(CGameItem ** items, int count);

/*
* Function: spiderFight
* Usage: spiderFight()
* -----------------------------------
* Allows the user to fight the spider
*/
void spiderFight(CGameItem ** items, int count);

/*
* Function: darkWizardFight
* Usage: darkWizardFight()
* -----------------------------------
* Allows the user to fight the spider
*/
void darkWizardFight(CGameItem ** items, int count);

/*
* Function: rockGolemFight
* Usage: rockGolemFight()
* -----------------------------------
* Allows the user to fight the spider
*/
void rockGolemFight(CGameItem ** items, int count);

/*
* Function: cyclopsFight
* Usage: cyclopsFight()
* -----------------------------------
* Allows the user to fight the spider
*/
void cyclopsFight(CGameItem ** items, int count);

/*
* Function: itemStoreMenu
* Usage: itemStoreMenu()
* -----------------------------------
* Prints the item store menu to the console.
*/
void itemStoreMenu();

/*
* Function: fightSelector
* Usage: fightSelector()
* -----------------------------------
* Allows the user to decide who to fight
*/
void fightSelector();

/*
* Function: fightMenu
* Usage: fightMenu()
* -----------------------------------
* Prints the fight menu to the console.
*/
void fightMenu();

int main()
{

	CGameItem * items[MAX_ITEMS]; // array of pointers 
	CGameItem * itemToAdd = NULL;

	int count = 0; // no weapons yet 
	int input;

	// no weapons yet
	for (int i = 0; i < MAX_ITEMS; i++)
	{
		items[i] = 0; // all NULL pointers
	}

	mainMenu();

	while (1)
	{
		cout << ": ";
		input = _getche();
		cout << endl;

		switch (input)
		{

		case 'i': //ITEM STORE
		case 'I':
			itemStore(items, itemToAdd, MAX_ITEMS, count);
			break;

		case 'g': //GO FIGHT
		case 'G':
			goFight(items, count);
			break;

		case 'Q': // QUIT
		case 'q':
			return 0; // end program

		default:
			mainMenu();

		}// end switch
	}

	system("pause");

	return 0;
}


void mainMenu()
{
	cout << endl;
	cout << "Where would you like to go?" << endl;
	cout << "  I = Item Store" << endl;
	cout << "  G = Go fight something!" << endl;
	cout << "  Q = Quit" << endl;
}

void itemStoreMenu()
{
	cout << endl;
	cout << "Welcome to the Item Store!" << endl;
	cout << "  A = Add an item" << endl;
	cout << "  L = List current items" << endl;
	cout << "  D = Drop off an item" << endl;
	cout << "  M = Main menu" << endl;
	cout << "What would you like to do?" << endl;
}

void fightSelector()
{
	cout << endl;
	cout << "Choose a challenge!" << endl;
	cout << "  C = Fight the cyclops" << endl;
	cout << "  D = Fight the dragon" << endl;
	cout << "  R = Fight the rock golem" << endl;
	cout << "  S = Fight the spider" << endl;
	cout << "  W = Fight the dark wizard" << endl;
	cout << "  M = Main menu" << endl;
}

void fightMenu()
{
	cout << endl;
	cout << "  U = Use an item" << endl;
	cout << "  L = List current items" << endl;
	cout << "  M = Main menu" << endl;
}

CGameItem * getItemToAdd()
{
	CGameItem * addMe = NULL;

	cout << endl;
	cout << "available weapons: " << endl;

	// use enum to output numbers
	cout << SPELL << ". Spell" << endl;
	cout << POTION << ". Potion" << endl;
	cout << SWORD << ". Sword" << endl;
	cout << SHIELD << ". Shield" << endl;
	cout << BOMB << ". Bomb" << endl;
	cout << BOW << ". Bow" << endl;
	cout << TORCH << ". Torch" << endl;

	// TODO: Add more enums to available items

	int choice = 0;

	cout << "Enter a choice: ";
	if (!getNumber(choice)) // could loop here until input is valid 
	{
		cout << "invalid input." << endl;
		return addMe; // return NULL
	}

	// TODO: 
	// use a switch statement to find the user choice -- use the enum in the switch statement
	// create object based on user input
	switch (choice)
	{
	case SPELL: addMe = new CSpell();
		break;
	case POTION: addMe = new CPotion();
		break;
	case SWORD: addMe = new CSword();
		break;
	case SHIELD: addMe = new CShield();
		break;
	case BOMB: addMe = new CBomb();
		break;
	case BOW: addMe = new CBow();
		break;
	case TORCH: addMe = new CTorch();
		break;
	}

	return addMe;
}

bool getNumber(int & number)
{
	// no code to add here 

	string input;

	getline(cin, input);

	// check that input is numeric
	for (unsigned int i = 0; i < input.length(); i++)
	{
		if (input[i] < '0' || input[i] > '9')
		{
			return false;
		}
	}

	number = atoi(input.c_str());

	return true;
}

bool add(CGameItem ** items, CGameItem * itemToAdd, int max, int & count)
{
	// TODO: 
	// if count >= max return false; 
	if ( count >= max )
	{
		cout << "\nYou can't carry any more items!" << endl;
		return false;
	}

	// assign object to array at count
	items[count] = itemToAdd;

	// increment count
	count++;

	return true; // everything worked OK
}

bool remove(CGameItem ** items, int & count, int index)
{
	// TODO: 
	// if index < 0 OR index >= count, return false
	if (index < 0 || index >= count)
	{
		cout << "\nYou don't have anything to drop!" << endl;
		return false;
	}

	// delete the item at index
	delete items[index];

	// shift all items over:
	// items[index] = items[index + 1]
	// index++
	// keep shifting while index < (count - 1)
	for (index; index < count - 1; index++)
	{
		items[index] = items[index + 1];
	}

	// decrement count (it's pass by reference and we want to return the new count)
	count--;

	return true; // item removed OK
}

void showItems(CGameItem ** items, int count)
{
	cout << endl;
	// no code to add here
	for (int i = 0; i < count; i++)
	{
		// pointer to pointer usage: The array name is the address of the 
		// array and can be stored in a pointer.  In this case, the array
		// is an array of pointers, so the array name is a pointer that
		// points to pointers and is, therefore, a pointer to a pointer. 
		// Note that there are two levels of dereferencing, the [] and the -> operators. 

		if (count == 0)
		{
			cout << "You don't have any items!\n";
		}

		else
		{
			cout << i << "." << items[i]->description() << endl;
		}
	}
	cout << endl;
	system("pause");
}

void itemStore(CGameItem ** items, CGameItem * itemToAdd, int max, int & count)
{
	int input;
	bool loop = true;

	while (loop)
	{
		system("cls");

		itemStoreMenu();
		cout << ": ";
		input = _getche();
		cout << endl;

		switch (input)
		{
		case 'a': // ADD
		case 'A':

			itemToAdd = getItemToAdd();

			if (itemToAdd != NULL)
			{
				add(items, itemToAdd, MAX_ITEMS, count);
			}
			break;

		case 'l': // LIST
		case 'L':
			showItems(items, count);
			break;

		case 'd': // REMOVE
		case 'D':

			cout << "Enter index to remove: ";

			if (getNumber(input) == false)
			{
				cout << "Invalid input" << endl;
				break; // get out of switch statement
			}

			remove(items, count, input);
			break;

		case 'm': //MAIN MENU
		case 'M':
			loop = false;
			system("cls");
			mainMenu();
			break;

		default: 
			system("cls");
			itemStoreMenu();
		}
	}
}

void goFight(CGameItem ** items, int count)
{
	int input;
	bool loop = true;

	while (loop)
	{
		system("cls");

		fightSelector();
		cout << ": ";
		input = _getche();
		cout << endl;

		switch (input)
		{
		case 'c': //CYCLOPS FIGHT
		case 'C':
			cyclopsFight(items, count);
			break;

		case 'd': //DRAGON FIGHT
		case 'D':
			dragonFight(items, count);
			loop = false;
			break;

		case 'r': //ROCK GOLEM FIGHT
		case 'R':
			rockGolemFight(items, count);
			loop = false;
			break;

		case 's': //SPIDER FIGHT
		case 'S':
			spiderFight(items, count);
			loop = false;
			break;

		case 'w': //DARK WIZARD FIGHT
		case 'W':
			darkWizardFight(items, count);
			loop = false;
			break;

		case 'm': //MAIN MENU
		case 'M':
			loop = false;
			system("cls");
			mainMenu();
			break;

		default: 
			system("cls");
			fightSelector();
		}
	}
}

void dragonFight(CGameItem ** items, int count)
{
	int input;
	int choice;
	int attempts = 3;
	bool loop = true;

	while (loop)
	{
		fightMenu();
		cout << ": ";
		input = _getche();
		cout << endl;

		switch (input)
		{

		case 'l': // LIST
		case 'L':
			showItems(items, count);
			break;

		case 'u': // USE
		case 'U':
			cout << "Enter index of item to use: ";
			if (getNumber(choice) == false)
			{
				cout << "Invalid input" << endl;
				break; // get out of switch statement
			}

			if (dynamic_cast<CSword*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "You killed the dragon!" << endl << endl;
				system("pause");
				loop = false;
			}
			
			else if (dynamic_cast<CShield*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "You raise your shield just in time to stop the dragon's firey breath!\n";
				cout << "You will not be charged an attempt\n\n";
				system("pause");
			}

			else if (dynamic_cast<CBomb*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "Everyone knows bombs don't work on dragons!\n\n";
				attempts -= 1;

				if (attempts <= 0)
				{
					cout << "The dragon is tired of your shenanigans\n";
					cout << "He gobbles you down for dinner!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the dragon\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CBow*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "The dragon shrugs off your arrows.\n\n";
				attempts -= 1;

				if (attempts <= 0)
				{
					cout << "The dragon is tired of your shenanigans\n";
					cout << "He gobbles you down for dinner!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the dragon\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CTorch*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "Really? Fire against a dragon? For that you lose 2 attempts\n\n";
				attempts -= 2;

				if (attempts <= 0)
				{
					cout << "The dragon is tired of your shenanigans\n";
					cout << "He gobbles you down for dinner!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the dragon\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CSpell*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "Spells don't work against dragons!\n\n";
				attempts -= 1;

				if (attempts <= 0)
				{
					cout << "The dragon is tired of your shenanigans\n";
					cout << "He gobbles you down for dinner!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the dragon\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CPotion*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "Drinking the potion restored an attempt!\n\n";
				attempts += 1;
			}
			
			else
			{
				cout << "index out of range or no items yet" << endl;
			}
			break;

		case 'm':
		case 'M':
			loop = false;
			break;

		default: fightMenu();
			break;
		}
	}
}

void spiderFight(CGameItem ** items, int count)
{
	int input;
	int choice;
	int attempts = 3;
	bool loop = true;

	while (loop)
	{
		fightMenu();
		cout << ": ";
		input = _getche();
		cout << endl;

		switch (input)
		{

		case 'l': // LIST
		case 'L':
			showItems(items, count);
			break;

		case 'u': // USE
		case 'U':
			cout << "Enter index of item to use: ";
			if (getNumber(choice) == false)
			{
				cout << "Invalid input" << endl;
				break; // get out of switch statement
			}

			if (dynamic_cast<CTorch*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "The spider recoils in fear as you drive the torch towards it!\n";
				cout << "The webbing surrounding the spider ignites and the spider falls victim to the flames!\n" << endl;
				system("pause");
				loop = false;
			}

			else if (dynamic_cast<CSword*>(items[choice]) == nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "The spider deflects your sword and strikes back!" << endl << endl;
				attempts -= 1;

				if (attempts <= 0)
				{
					cout << "The spider cocoons you in his web!\n";
					cout << "He drains your innards for dinner!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the spider\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CShield*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "You raise your shield just in time to stop the spider's sticky webbing!\n";
				cout << "You will not be charged an attempt\n\n";
				system("pause");
			}

			else if (dynamic_cast<CBomb*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "The spider webs the bomb's fuse before it can go off!\n\n";
				attempts -= 1;

				if (attempts <= 0)
				{
					cout << "The spider cocoons you in his web!\n";
					cout << "He drains your innards for dinner!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the spider\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CBow*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "The spider's hard exoskeleton deflects the arrows.\n\n";
				attempts -= 1;

				if (attempts <= 0)
				{
					cout << "The spider cocoons you in his web!\n";
					cout << "He drains your innards for dinner!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the spider\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CSpell*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "Spells don't work against dragons!\n\n";
				attempts -= 1;

				if (attempts <= 0)
				{
					cout << "The spider cocoons you in his web!\n";
					cout << "He drains your innards for dinner!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the spider\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CPotion*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "Drinking the potion restored an attempt!\n\n";
				attempts += 1;
			}

			else
			{
				cout << "index out of range or no items yet" << endl;
			}
			break;

		case 'm':
		case 'M':
			loop = false;
			break;

		default: fightMenu();
			break;
		}
	}	
}

void darkWizardFight(CGameItem ** items, int count)
{
	int input;
	int choice;
	int attempts = 3;
	bool loop = true;

	while (loop)
	{
		fightMenu();
		cout << ": ";
		input = _getche();
		cout << endl;

		switch (input)
		{

		case 'l': // LIST
		case 'L':
			showItems(items, count);
			break;

		case 'u': // USE
		case 'U':
			cout << "Enter index of item to use: ";
			if (getNumber(choice) == false)
			{
				cout << "Invalid input" << endl;
				break; // get out of switch statement
			}

			if (dynamic_cast<CSpell*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "Your magic overpowered the Dark Wizard's!\n";
				cout << "There's a bright flash and all that remains of the Wizard is\n";
				cout << "smoldering pile of ash!" << endl << endl;
				system("pause");
				loop = false;
			}

			else if (dynamic_cast<CTorch*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "The wizard uses his magic to engulf you in your own flames!" << endl;
				cout << "The sever burns from the fire cause you to lose 2 attemps!" << endl << endl;
				attempts -= 2;

				if (attempts <= 0)
				{
					cout << "The Dark Wizard banishes you to another dimension!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the Dark Wizard\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CSword*>(items[choice]) == nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "The Dark Wizard uses magic to deflects your sword and strikes back!" << endl << endl;
				attempts -= 1;

				if (attempts <= 0)
				{
					cout << "The Dark Wizard banishes you to another dimension!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the Dark Wizard\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CShield*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "You raise your shield just in time to deflect the Dark Wizard's magic!\n";
				cout << "You will not be charged an attempt\n\n";
				system("pause");
			}

			else if (dynamic_cast<CBomb*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "The Dark Wizard makes the bomb vanish!\n\n";
				attempts -= 1;

				if (attempts <= 0)
				{
					cout << "The Dark Wizard banishes you to another dimension!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the Dark Wizard\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CBow*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "The Dark Wizard's uses black magic to disintegrate your arrows!\n\n";
				attempts -= 1;

				if (attempts <= 0)
				{
					cout << "The Dark Wizard banishes you to another dimension!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the Dark Wizard\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CPotion*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "Drinking the potion restored an attempt!\n\n";
				attempts += 1;
			}

			else
			{
				cout << "index out of range or no items yet" << endl;
			}
			break;

		case 'm':
		case 'M':
			loop = false;
			break;

		default: fightMenu();
			break;
		}
	}
}

void rockGolemFight(CGameItem ** items, int count)
{
	int input;
	int choice;
	int attempts = 3;
	bool loop = true;

	while (loop)
	{
		fightMenu();
		cout << ": ";
		input = _getche();
		cout << endl;

		switch (input)
		{

		case 'l': // LIST
		case 'L':
			showItems(items, count);
			break;

		case 'u': // USE
		case 'U':
			cout << "Enter index of item to use: ";
			if (getNumber(choice) == false)
			{
				cout << "Invalid input" << endl;
				break; // get out of switch statement
			}

			if (dynamic_cast<CBomb*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "Your bomb blew the rock golem to rubble!\n\n";
				system("pause");
				loop = false;
			}

			else if (dynamic_cast<CSpell*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "Your magic has no effect on the hard rock shell of the golem!" << endl << endl;
				attempts -= 1;

				if (attempts <= 0)
				{
					cout << "The rock golem crushes you to pieces!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the rock golem\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CTorch*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "The rock golem is impervious to your flames!" << endl << endl;
				attempts -= 1;

				if (attempts <= 0)
				{
					cout << "The rock golem crushes you to pieces!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the rock golem\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CSword*>(items[choice]) == nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "Not even a scratch!" << endl << endl;
				attempts -= 1;

				if (attempts <= 0)
				{
					cout << "The rock golem crushes you to pieces!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the rock golem\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CShield*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "You raise your shield just in time to deflect the Dark Wizard's magic!\n";
				cout << "You will not be charged an attempt\n\n";
				system("pause");
			}

			else if (dynamic_cast<CBow*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "Your arrows bounce harmlessly off of the rock golem!\n\n";
				attempts -= 1;

				if (attempts <= 0)
				{
					cout << "The rock golem crushes you to pieces!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the rock golem\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CPotion*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "Drinking the potion restored an attempt!\n\n";
				attempts += 1;
			}

			else
			{
				cout << "index out of range or no items yet" << endl;
			}
			break;

		case 'm':
		case 'M':
			loop = false;
			break;

		default: fightMenu();
			break;
		}
	}
}

void cyclopsFight(CGameItem ** items, int count)
{
	int input;
	int choice;
	int attempts = 3;
	bool loop = true;

	while (loop)
	{
		fightMenu();
		cout << ": ";
		input = _getche();
		cout << endl;

		switch (input)
		{

		case 'l': // LIST
		case 'L':
			showItems(items, count);
			break;

		case 'u': // USE
		case 'U':
			cout << "Enter index of item to use: ";
			if (getNumber(choice) == false)
			{
				cout << "Invalid input" << endl;
				break; // get out of switch statement
			}

			if (dynamic_cast<CBow*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "Your arrow flew true and struck the cyclops right in the eye!\n";
				cout << "The cyclops falls to the ground with a mighty crash!" << endl << endl;
				system("pause");
				loop = false;
			}

			else if (dynamic_cast<CBomb*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "The cyclops grabs your bomb and swallows it!" << endl;
				cout << "He laughs as it goes off in his stomach... and then laughs harder!" << endl << endl;
				attempts -= 1;

				if (attempts <= 0)
				{
					cout << "The cyclops grabs you and swallows you whole!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the cyclops\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CSpell*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "Your magic deflects of the impenetrable skin of the cyclops!" << endl << endl;
				attempts -= 1;

				if (attempts <= 0)
				{
					cout << "The cyclops grabs you and swallows you whole!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the cyclops\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CTorch*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "The cyclops recoils at the flames but no more!" << endl;
				cout << "You are not charged an attempt" << endl << endl;
				system("pause");
			}

			else if (dynamic_cast<CSword*>(items[choice]) == nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "Your sword can't cut deep enough to hurt the cylops!" << endl << endl;
				attempts -= 1;

				if (attempts <= 0)
				{
					cout << "The cyclops grabs you and swallows you whole!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the cyclops\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CShield*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "Your shield can't withstand the mighty blow from the cyclops!" << endl;
				cout << "The blow is so strong it costs you two attempts" << endl << endl;
				attempts -= 2;

				if (attempts <= 0)
				{
					cout << "The cyclops grabs you and swallows you whole!\n\n";
					loop = false;
				}
				else
				{
					cout << "You have " << attempts << " attempts left to kill the cyclops\n\n";
					system("pause");
				}
			}

			else if (dynamic_cast<CPotion*>(items[choice]) != nullptr)
			{
				cout << endl;
				items[choice]->use();
				cout << endl;
				cout << "Drinking the potion restored an attempt!!\n\n";
				attempts += 1;
				system("pause");
			}

			else
			{
				cout << "index out of range or no items yet" << endl;
			}
			break;

		case 'm':
		case 'M':
			loop = false;
			break;

		default: fightMenu();
			break;
		}
	}
}
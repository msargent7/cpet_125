/*
* CGameItem.cpp
* ---------------------------------------------
* Implementation for items
*
*/

#include "CGameItem.h"

// CSpell
CSpell::CSpell()
{
	_value = 5;
	_description = "Spell";
}

CSpell::CSpell(CSpell & right)
{
	_value = right._value;
	_description = right._description;
}

void CSpell::use()
{
	cout << "Expecto Patronum!" << endl;
}


// CPotion
CPotion::CPotion()
{
	_value = 2;
	_description = "Potion";
}

CPotion::CPotion(CPotion  & right)
{
	_value = right._value;
	_description = right._description;
}

void CPotion::use()
{
	cout << "Glug glug glug" << endl;
}

// CSword
CSword::CSword()
{
	_value = 2;
	_description = "Sword";
}

CSword::CSword(CSword  & right)
{
	_value = right._value;
	_description = right._description;
}

void CSword::use()
{
	cout << "Hiyaah!" << endl;
}

// CShield
CShield::CShield()
{
	_value = 2;
	_description = "Shield";
}

CShield::CShield(CShield  & right)
{
	_value = right._value;
	_description = right._description;
}

void CShield::use()
{
	cout << "Take cover!" << endl;
}

// CBow
CBow::CBow()
{
	_value = 2;
	_description = "Bow and arrows";
}

CBow::CBow(CBow  & right)
{
	_value = right._value;
	_description = right._description;
}

void CBow::use()
{
	cout << "Thwp thwp thwp" << endl;
}

// CBomb
CBomb::CBomb()
{
	_value = 2;
	_description = "Bombs";
}

CBomb::CBomb(CBomb  & right)
{
	_value = right._value;
	_description = right._description;
}

void CBomb::use()
{
	cout << "Ka-BOOM!!" << endl;
}

// CTorch
CTorch::CTorch()
{
	_value = 2;
	_description = "Bombs";
}

CTorch::CTorch(CTorch  & right)
{
	_value = right._value;
	_description = right._description;
}

void CTorch::use()
{
	cout << "Crackle crackle crackle" << endl;
}
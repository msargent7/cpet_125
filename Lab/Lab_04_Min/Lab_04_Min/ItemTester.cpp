/*
* ItemTester.cpp
* ---------------------------------------------------
* Tester for CGameItem derived classes.
*
* The student should modify the following functions:
*
*	getItemToAdd
*	add
*	remove
*
* The student should also modify the enum
*
* Suggest you start by getting the add function to work and test.
* Next, get the remove function to work.
* Next, add your own classes.
* Test your classes by adding to the array of pointers.
*
* Challenge yourself:	Remove an item if it used up - create your own rules for this
*						Let the player encounter obstacles and require items
*						Further limit number of items player can have so it is harder to complete the game
*						Create a class the stores the array of pointers (Do not use the STL; it does not work well with pointers)
*
*/

/*
FILE: ItemTester.cpp
PROGRAMMER: <your name>
DATE: <the date the project is submitted>
PROJECT: AG121_Lab04
CREDITS: <A list of web sites, books, and programmers used to create the code.
You do not need to list any text book used in this course or the instructors name. >
LIMITATIONS: <Any limitations your program may have.  For example, Your  program might fail if the user inputs a number instead of a string.>
*/

#include <iostream>
#include <string>
using namespace std;

#include <conio.h>

#include "CGameItem.h"

// TODO: Add choices to the enum
enum INPUT_CHOICES {
	SPELL,
	POTION,
	SWORD,
	SHIELD,
	BOW,
	BOMB,
	TORCH

	// TODO: Add to enum
};

const int MAX_ITEMS = 5;

/*
* Function: help
* Usage: help()
* -----------------------------------
* Prints the help menu to the console.
*/
void help();

/*
* Function: getItemToAdd
* Usage: CGameItem * w = getItemToAdd();
* -----------------------------------
* If successful, this function returns a valid pointer
* to an object derived from type CGameItem.  If the  function
* fails, the function returns NULL.
*/
CGameItem * getItemToAdd();

/*
* Function: getNumber
* Usage: bool result = getNumber(number);
* -----------------------------------
* This function gets a number input by the user.  If the user
* inputs a number, the function returns true and sets number
* the the value input by the user.  If the function returns false,
* the value of number is not changed.
*
*/
bool getNumber(int & number);

/*
* Function: add
* Usage: add(weapons, w, MAX, count)
* -----------------------------------
* This function adds a valid pointer to an array of CWeapon
* pointers.  If successful, the function returns true and the
* value of count is the number of weapons in the pointer array.
* If the function fails, it returns false and the value of count
* is unchanged.
*/
bool add(CGameItem ** items, CGameItem * itemToAdd, int max, int & count);

/*
* Function: remove
* Usage: remove(items, count, index)
* -----------------------------------
* If successful, this function deletes the item at index and
* sets count to the new count of items in the array.  Otherwise,
* the function returns false and count is unchanged.
*/
bool remove(CGameItem ** items, int & count, int index);

/*
* Function: showItems
* Usage: showItems(items, count)
* -----------------------------------
* Prints a list of item descriptions to the console.
*/
void showItems(CGameItem ** items, int count);

int main()
{

	CGameItem * items[MAX_ITEMS]; // array of pointers 
	CGameItem * itemToAdd = NULL;

	int count = 0; // no weapons yet 
	int input;

	// no weapons yet
	for (int i = 0; i < MAX_ITEMS; i++)
	{
		items[i] = 0; // all NULL pointers
	}

	help();

	while (1)
	{
		cout << ">";
		input = _getche();
		cout << endl;

		switch (input)
		{
		case 'a': // ADD
		case 'A':

			itemToAdd = getItemToAdd();

			if (itemToAdd != NULL)
			{
				add(items, itemToAdd, MAX_ITEMS, count);
			}

			break;

		case 'l': // LIST
		case 'L':

			showItems(items, count);
			break;

		case 'Q': // QUIT
		case 'q':
			return 0; // end program

		case 'r': // REMOVE
		case 'R':

			cout << "Enter index to remove: ";

			if (getNumber(input) == false)
			{
				cout << "Invalid input" << endl;
				break; // get out of switch statement
			}

			remove(items, count, input);

			break;

		case 'u': // USE
		case 'U':

			cout << "Enter index of item to use: ";
			if (getNumber(input) == false)
			{
				cout << "Invalid input" << endl;
				break; // get out of switch statement
			}

			if (input >= 0 && input < count)
			{
				items[input]->use();
			}
			else
			{
				cout << "index out of range or no items yet" << endl;
			}

			break;

		default:
			help();

		}// end switch
	}

	system("pause");

	return 0;
}

void help()
{
	cout << endl;
	cout << "Menu: " << endl;
	cout << "  a = add" << endl;
	cout << "  h = help" << endl;
	cout << "  l = list items" << endl;
	cout << "  q = quit" << endl;
	cout << "  r = remove" << endl;
	cout << "  u = use" << endl;
}

CGameItem * getItemToAdd()
{
	CGameItem * addMe = NULL;

	cout << endl;
	cout << "available weapons: " << endl;

	// use enum to output numbers
	cout << SPELL << ". Spell" << endl;
	cout << POTION << ". Potion" << endl;
	cout << SWORD << ". Sword" << endl;
	cout << SHIELD << ". Shield" << endl;
	cout << BOW << ". Bow" << endl;
	cout << BOMB << ". Bomb" << endl;
	cout << TORCH << ". Torch" << endl;

	// TODO: Add more enums to available items

	int choice = 0;

	cout << "Enter a choice: ";
	if (!getNumber(choice)) // could loop here until input is valid 
	{
		cout << "invalid input." << endl;
		return addMe; // return NULL
	}

	// TODO: 
	// use a switch statement to find the user choice -- use the enum in the switch statement
	// create object based on user input
	switch (choice)
	{
	case SPELL: addMe = new CSpell();
		break;
	case POTION: addMe = new CPotion();
		break;
	case SWORD: addMe = new CSword();
		break;
	case SHIELD: addMe = new CShield();
		break;
	case BOW: addMe = new CBow();
		break;
	case BOMB: addMe = new CBomb();
		break;
	case TORCH: addMe = new CTorch();
		break;
	}


	return addMe;
}

bool getNumber(int & number)
{
	// no code to add here 

	string input;

	getline(cin, input);

	// check that input is numeric
	for (unsigned int i = 0; i < input.length(); i++)
	{
		if (input[i] < '0' || input[i] > '9')
		{
			return false;
		}
	}

	number = atoi(input.c_str());

	return true;
}

bool add(CGameItem ** items, CGameItem * itemToAdd, int max, int & count)
{
	// TODO: 
	// if count >= max return false;
	if (count >= max)
	{
		cout << "\nYou can't carry any more items!" << endl;
		return false;
	}

	// assign object to array at count
	items[count] = itemToAdd;

	// increment count
	count++;

	return true; // everything worked OK
}

bool remove(CGameItem ** items, int & count, int index)
{
	// TODO: 
	// if index < 0 OR index >= count, return false
	if (index < 0 || index >= count)
	{
		cout << "\nYou don't have anything to drop!" << endl;
		return false;
	}

	// delete the item at index
	delete items[index];

	// shift all items over:
	// items[index] = items[index + 1]
	// index++
	// keep shifting while index < (count - 1)
	for (index; index < count - 1; index++)
	{
		items[index] = items[index + 1];
	}

	// decrement count (it's pass by reference and we want to return the new count)
	count--;

	return true; // item removed OK
}

void showItems(CGameItem ** items, int count)
{
	// no code to add here
	for (int i = 0; i < count; i++)
	{
		// pointer to pointer usage: The array name is the address of the 
		// array and can be stored in a pointer.  In this case, the array
		// is an array of pointers, so the array name is a pointer that
		// points to pointers and is, therefore, a pointer to a pointer. 
		// Note that there are two levels of dereferencing, the [] and the -> operators. 

		cout << i << "." << items[i]->description() << endl;

	}
}


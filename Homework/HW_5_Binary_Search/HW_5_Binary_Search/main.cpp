#include <iostream>
using namespace std;

int binarySearch(int arr[], int first, int last, int key);

int main()
{
	const int SIZE = 9;
	int arr[SIZE];
	int searchResult;
	int first = 0;
	int key;

	cout << "What number would you like to search for?\n";
	cin >> key;
	cout << endl;

	for (int i = 0, j = 7; i < SIZE; i++, j++)
	{
		arr[i] = j;
	}
	
	searchResult = binarySearch(arr, first, SIZE, key);

	if (searchResult == -1)
	{
		cout << "That number is not contained in the array\n\n";
	}

	else
	{
		cout << "Your number was found!\n";
		cout << key << " is at index " << searchResult << " in the array!" << endl;
	}

	system("pause");
	return 0;
}

int binarySearch(int arr[], int first, int last, int key)
{
	int mid = (first + last) / 2;
	int index;

	if (first > last)
	{
		index = -1;
		return index;
	}
	
	if (key == arr[mid])
	{
		index = mid;
		return index;
	}

	else if (key > arr[mid])
	{
		index = binarySearch(arr, mid + 1, last, key);
	}

	else if (key < arr[mid])
	{
		index = binarySearch(arr, first, mid - 1, key);
	}
}
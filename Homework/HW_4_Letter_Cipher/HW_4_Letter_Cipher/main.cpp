#include <iostream>
#include <string>
using namespace std;

string EncodeString(string str, int key);
string decodeString(string str, int key);

int main()
{
	string str;
	string codedStr;
	int key;

	cout << "Programmed by Michael Sargent\n\n";

	cout << "Put in a string that you'd like to encode: \n";
	getline(cin, str);
	cout << endl;

	cout << "Now enter a key to shift the letters by: \n";
	cin >> key;
	cout << endl;

	cout << "The encrypted message is: \n";
	codedStr = EncodeString(str, key);
	cout << codedStr << endl;


	system("pause");
	return 0;
}

string EncodeString(string str, int key)
{
	int length;
	string newStr = str;

	length = (int)str.length();

	for (int i = 0; i < length; i++)
	{
		if (isalpha(str[i]))
		{
			newStr[i] = str[i] + key;

			if (newStr[i] > 90 && newStr[i] < 97)
			{
				newStr[i] -= 26;
			}
			else if (newStr[i] > 122)
			{
				newStr[i] -= 26;
			}
			else
				newStr[i] = newStr[i];
		}
		else
		{
			newStr[i] = str[i];
		}
	}
	return newStr;
}

string decodeString(string str, int key)
{
	int length;
	string newStr = str;

	length = (int)str.length();

	for (int i = 0; i < length; i++)
	{
		if (isalpha(str[i]))
		{
			newStr[i] = str[i] - key;

			if (newStr[i] > 90 && newStr[i] < 97)
			{
				newStr[i] -= 26;
			}
			else if (newStr[i] > 122)
			{
				newStr[i] -= 26;
			}
			else
				newStr[i] = newStr[i];
		}
		else
		{
			newStr[i] = str[i];
		}
	}
	return newStr;
}

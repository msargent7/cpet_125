#include <iostream>
using namespace std;

void RemoveZeroElements(int arr[], int &n);

int main()
{

	const int SIZE = 13;
	int scores[SIZE] = { 65, 0, 95, 0, 0,
							79, 82, 0, 84, 84,
							86, 90, 0 };
	int n = SIZE;

	cout << "Programmed By: Michael Sargent \n" << endl;
	cout << "Your test scores this semester were: \n";
	for (int i = 0; i < n; i++)
	{
		if (i == n - 1)
		{
			cout << scores[i] << "\n\n";
		}
		else
		{
			cout << scores[i] << ", ";
		}
	}

	cout << "Now removing all of your zero scores...\n\n";
	RemoveZeroElements(scores, n);
	cout << "Without the zeros, there were " << n << " scores.\n";
	cout << "Those scores were: \n";
	for (int i = 0; i < n; i++)
	{
		if (i == n - 1)
		{
			cout << scores[i] << "\n\n";
		}
		else
		{
			cout << scores[i] << ", ";
		}
	}

	system("pause");
	return 0;
}

void RemoveZeroElements(int arr[], int &n)
{
	int newSize = 0;
	for (int i = 0; i < n; i++)
	{
		if (arr[i] == 0)
		{
			for (int j = i + 1; j < n; j++)
			{
				if (arr[j] != 0)
				{
					arr[i] = arr[j];
					arr[j] = 0;
					newSize++;
					break;
				}
			}
		}
	}

	n = newSize + 1;
}
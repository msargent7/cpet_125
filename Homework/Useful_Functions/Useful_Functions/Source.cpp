#include <iostream>
#include <ctime>
using namespace std;

void swap(int *a, int *b);
void selectionSort(int *arr, int size);

int main()
{
	const int MAX = 100;
	int a = 7;
	int b = 9;
	int i;
	int arr[MAX] = { 0 };

	swap(&a, &b);
	cout << a << " " << b << endl << endl;

	srand(time(0));
	for (i = 0; i < MAX; i++)
	{
		arr[i] = rand();
	}

	selectionSort(arr, MAX);
	for (i = 0; i < MAX; i++)
	{
		if (i == MAX - 1)
		{
			cout << arr[i] << endl << endl;
		}
		else
		{
			cout << arr[i] << ", ";
		}
	}
	

	system("pause");
	return 0;
}

void swap(int *a, int *b) {

	int temp;

	temp = *a;
	*a = *b;
	*b = temp;

}

void selectionSort(int *arr, int size) {

	int lh, rh, i;

	for (lh = 0; lh < size; lh++)
	{
		rh = lh;
		for (i = lh + 1; i < size; i++)
		{
			if (arr[i] < arr[rh])
			{
				rh = i;
			}
			swap(&arr[lh], &arr[rh]);
		}
	}

}
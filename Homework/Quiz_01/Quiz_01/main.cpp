#include <iostream>
using namespace std;

int f(char * x)
{
	if (*x) return f(x + 1) + 1;

	return 0;
}

template <typename T>
void s(T & a, T & b)
{
	T t = a;
	a = b;
	b = t;
}

template <typename T>
void s(T * x, int a, int b)
{
	if (a >= b) return;

	s(x[a], x[b]);

	s(x, a + 1, b - 1);
}

bool fd(char * s, char m)
{
	if (*s) return (*s == m) ? true : fd(s + 1, m);

	return false;
}

int main()
{



	return 0;
}
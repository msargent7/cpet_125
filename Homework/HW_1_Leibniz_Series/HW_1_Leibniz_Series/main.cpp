#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
	double total = 0;
	int count = 0;

	cout << "Programmed By: Michael Sargent\n" << endl;
	cout << "This program uses the first 10000 terms in the Leibniz \n";
	cout << "to calculate 'pi / 4' \n" << endl;

	for (double i = 0; i < 10000; i++)
	{
		total += (pow(-1, i) / ((2 * i) + 1));
		count++;
	}
	cout << "Te result of the sequence is: " << endl;
	cout << setprecision(4) << fixed << total << "\n" << endl;

	system("pause");
	return 0;
}

/* Proof

Programmed By: Michael Sargent

This program uses the first 10000 terms in the Leibniz
to calculate 'pi / 4'

Te result of the sequence is:
0.7854

Press any key to continue . . .

*/
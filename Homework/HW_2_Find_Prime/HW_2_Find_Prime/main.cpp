#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;


void sieve(int n, int A[]);

int main()
{

	int const MAX = 1000;

	cout << "Programmed By: Michael Sargent\n";
	cout << "This program uses the sieve of Eratostenes to determine\n";
	cout << "all of the prime numbers between 2 and " << MAX << "\n" << endl;
	
	int arr[MAX];
	int count = 0;

	sieve(MAX, arr);

	for (int i = 2; i < MAX; i++)
	{
		if (arr[i] == 1)
		{
			cout << i << " ";
			count++;
			if (count == 10)
			{
				cout << endl;
				count = 0;
			}
		}
	}
	cout << endl << endl;
	system("pause");
	return 0;
}

void sieve(int n, int A[])
{
	for (int i = 0; i < n; i++)
	{
		A[i] = 1;
	}
	A[0] = 0;
	A[1] = 1;
	for (int i = 2; i < sqrt(n); i++)
	{
		for (int j = i * i; j < n; j += i)
		{
			A[j] = 0;
		}
	}
}
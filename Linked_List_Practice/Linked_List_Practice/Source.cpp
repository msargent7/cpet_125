#include <iostream>
using namespace std;

typedef struct _NODE_ {
	_NODE_ * next;
	_NODE_ * prev;
	int data;
}NODE, *PNODE;

class linkedList {
private:

	NODE * _head;
	NODE * _tail;
	int _size;

public:

	linkedList() {
		_head = nullptr;
		_tail = nullptr;
		_size = 0;
	}

	~linkedList() {
		delete _head;
	}

	int size()
	{
		return _size;
	}

	NODE * createNode(int value)
	{
		NODE * temp = new NODE;
		temp->data = value;
		temp->next = nullptr;
		temp->prev = nullptr;

		return temp;
	}

	void pushFront(int value)
	{
		NODE * temp;

		temp = createNode(value);

		if (_head == nullptr)
		{
			_head = _tail = temp;
		}

		else
		{
			temp->next = _head;
			_head->prev = temp;
			_head = temp;
		}
		_size++;
	}

	void popFront()
	{

		if (_head == nullptr && _tail == nullptr)
		{
			cout << "The list is empty";
		}

		else if (_head->next == nullptr)
		{
			delete _tail;
			_tail = _head = nullptr;
		}

		else
		{
			NODE * temp;
			temp = _head;
			_head = _head->next;
			_head->prev = nullptr;
			delete temp;
		}

		_size--;
	}

	void printList()
	{
		NODE * temp = _head;

		if (temp == nullptr)
		{
			cout << "The list is empty" << endl;
			return;
		}

		while (temp != NULL)
		{
			cout << temp->data << endl;
			temp = temp->next;
		}
		cout << endl;
	}

	void reverse()
	{
		NODE * current;
		NODE * next;
		NODE * prev = NULL;
		NODE * revList;
		current = _head;

		while (current != NULL)
		{
			next = current->next;
			prev = current->prev;
			current->next = prev;
			prev = current;
			current = next;
		}

		revList = prev;

		NODE * temp = revList;

		while (temp != NULL)
		{
			cout << temp->data << endl;
			temp = temp->next;
		}
		cout << endl;
	}

	void pushBack(int value)
	{
		NODE * temp = createNode(value);

		if (_head == nullptr)
		{
			_head = _tail = temp;
		}

		else
		{
			temp->prev = _tail;
			_tail->next = temp;
			_tail = temp;
		}
		_size++;
	}

	void popBack()
	{
		if (_head == nullptr && _tail == nullptr)
		{
			return;
		}

		else if (_head == _tail)
		{
			delete _tail;
			_tail = _head = nullptr;
		}

		else
		{
			NODE * last = _tail;
			_tail = _tail->prev;
			delete last;
			_tail->next = nullptr;
		}
		_size--;
	}

	void clear()
	{
		NODE * temp = _head;

		while (_head != nullptr)
		{
			popFront();
		}
		_size = 0;
	}

	void insert(int position, int value)
	{
		NODE * temp = createNode(value);
		int count = 0;

		if (position == 0)
		{
			temp->next = _head;
			_head->prev = temp;
			_head = temp;
		}

		else if (position == _size - 1)
		{
			_tail->next = temp;
			temp->prev = _tail;
			_tail = temp;
		}

		else
		{
			NODE * current = _head;

			while (current->next != nullptr)
			{
				if (count == position)
				{
					temp->next = current;
					temp->prev = current->prev;
					current->prev = temp;
					return;
				}
				current = current->next;
				count++;
			}
		}
		_size++;
	}

	void erase(int position)
	{
		int count = 0;
		NODE * current = _head;

		if (position == 0)
		{
			_head = _head->next;
			_head->prev = nullptr;
		}

		else if (position == _size - 1)
		{
			_tail = _tail->prev;
			_tail->next = nullptr;
		}

		else
		{
			while (current->next != nullptr)
			{
				if (count == position)
				{
					current->prev->next = current->next;
					current->next->prev = current->prev;
				}
			}
			count++;
		}
		_size--;
	}

	void printListRecursive(NODE * pNode)
	{
		NODE * temp = pNode;

		if (temp == NULL)
		{
			cout << endl;
		}
		else
		{
			cout << temp->data << endl;
			temp = temp->next;
			printListRecursive(temp);
		}
	}

	NODE * begin()
	{
		return _head;
	}

	NODE * end()
	{
		return _tail;
	}

};


int main()
{
	linkedList list;
	NODE * head;
	NODE * tail;

	head = list.begin();
	tail = list.end();

	list.pushFront(5);
	list.pushFront(6);
	list.pushFront(7);
	list.pushFront(8);
	list.pushFront(9);
	list.pushBack(10);
	list.pushBack(11);
	list.printList();
	list.printListRecursive(head);

	list.popFront();
	list.popFront();
	list.popBack();

	list.printList();
	list.insert(1, 12);
	list.erase(2);

	list.printList();
	list.reverse();

	list.clear();
	list.printList();
	
}